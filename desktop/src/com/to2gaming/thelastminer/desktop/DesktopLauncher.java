package com.to2gaming.thelastminer.desktop;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import com.to2gaming.thelastminer.TheLastMinerGame;
import com.to2gaming.thelastminer.ui.util.UIConstants;

public class DesktopLauncher {
	public static void main (String[] arg) {
		LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
		config.height = UIConstants.SCREEN_HEIGHT;
		config.width = UIConstants.SCREEN_WIDTH;
		//todo unhardcode
		new LwjglApplication(new TheLastMinerGame(), config);
	}
}
