package com.to2gaming.thelastminer.mocks;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Queue;

/**
 * Created by Kuba on 03.01.2016.
 */
public class GridEnemyMock {

    ArrayList<int[]> nodes = new ArrayList<>();
    ArrayList<int[]> directions = new ArrayList<>();

    int[] dir = {1, 0};
    int[] dir2 = {0, 1};
    int[] dir3 = {-1, 0};
    int[] dir4 = {0, -1};


    public GridEnemyMock(int width, int height) {
        for (int x = 0; x < width; x += 1) {
            for (int y = 0; y < height; y += 1) {
                int[] tab = {x, y};
                nodes.add(tab);
            }
        }
        directions.add(dir);
        directions.add(dir2);
        directions.add(dir3);
        directions.add(dir4);

        LinkedList<int[]> frontier = new LinkedList<>();
        int[] center = {width/2,height/2};
        frontier.add(center);
        HashMap cameFrom= new HashMap();
        cameFrom.put(center,null);
        int[] current;
        while (!frontier.isEmpty()){
            current = frontier.getLast();
            for (int[] next : Neighbours(current)){
                if (!cameFrom.containsKey(next)){
                    frontier.add(next);
                    cameFrom.put(next, current);
                }
            }
        }
    }

    public ArrayList<int[]> Neighbours(int[] node) {
        ArrayList<int[]> neighbours = new ArrayList<>();
        for (int[] direction : directions) {
            int[] tab = {node[0] + direction[0], node[1] + direction[1]};
            if (nodes.contains(tab)) {
                neighbours.add(tab);
            }
        }
        return neighbours;
    }
}

