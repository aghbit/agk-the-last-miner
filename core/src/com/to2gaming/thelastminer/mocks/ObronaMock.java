package com.to2gaming.thelastminer.mocks;

import com.to2gaming.thelastminer.aimodules.enemyai.Enemy;
import com.to2gaming.thelastminer.aimodules.buildingsai.Building;
import com.to2gaming.thelastminer.manager.MinerList;

public class ObronaMock {
    private MinerList<Building> buildings = new MinerList<Building>();
    private MinerList<Enemy> enemies = new MinerList<Enemy>();

    private int GameX = 300;
    private int GameY = 200;

    boolean canMove(int x, int y) {
        return true;
    }

    void addEnemy(Enemy enemy) {
        enemies.add(enemy);
    }

    public void behave() {
        for (Object e : enemies) {
            ((Enemy) e).behave(buildings);
        }
    }
}