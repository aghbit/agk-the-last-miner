package com.to2gaming.thelastminer.mocks;

import com.to2gaming.thelastminer.aimodules.buildingsai.Building;
import com.to2gaming.thelastminer.phases.phaseDefence.AttackType;


public class Attack2 {
//    public Building getTarget() {
//	return new Building();
//    }
    
    public AttackType getAttackType() {
	return AttackType.ICE;
    }
    
    public Integer getValue() {
	return 10;
    }
}
