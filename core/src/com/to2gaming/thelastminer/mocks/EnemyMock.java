package com.to2gaming.thelastminer.mocks;

import com.to2gaming.thelastminer.aimodules.enemyai.Enemy;

/**
 * Created by pkoniu on 14/12/15.
 */
public class EnemyMock extends Enemy {
    public EnemyMock(float x, float y, int hp) {
        super(x, y);
        super.setHp(hp);
    }
}
