package com.to2gaming.thelastminer.mocks;

import com.to2gaming.thelastminer.aimodules.enemyai.Enemy;
import com.to2gaming.thelastminer.phases.phaseDefence.AttackType;

public class Attack {
//    public Enemy getTarget() {
//	    return new Enemy();
//    }
    
    public AttackType getAttackType() {
	    return AttackType.ICE;
    }
    
    public Integer getValue() {
	    return 10;
    }
}
