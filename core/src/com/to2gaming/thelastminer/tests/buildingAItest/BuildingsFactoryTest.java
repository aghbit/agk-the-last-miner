package com.to2gaming.thelastminer.tests.buildingAItest;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.to2gaming.thelastminer.aimodules.buildingsai.Building;
import com.to2gaming.thelastminer.phases.phaseBuilding.BuildingType;
import com.to2gaming.thelastminer.phases.phaseBuilding.BuildingsFactory;
import com.to2gaming.thelastminer.tests.GdxTestRunner;
import org.junit.*;
import org.junit.runner.RunWith;

import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;


/**
 * Created by Konrad on 2015-11-23.
 */
@RunWith(GdxTestRunner.class)
public class BuildingsFactoryTest {

    @Before
    public void setup() {
        Gdx.gl = mock(GL20.class);
    }

    @Test(expected=IllegalArgumentException.class)
    public void getNull() {
        Building b = BuildingsFactory.getBuilding(BuildingType.valueOf("nothing"));
    }

    @Test
    public void getNormalBuilding() {
        Building b = BuildingsFactory.getBuilding(BuildingType.MACHINE_GUN);
        assertTrue(b != null);
    }
}
