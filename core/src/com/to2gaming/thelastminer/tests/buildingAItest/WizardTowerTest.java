package com.to2gaming.thelastminer.tests.buildingAItest;

import com.to2gaming.thelastminer.aimodules.buildingsai.buildings.WizardTower;
import com.to2gaming.thelastminer.aimodules.enemyai.Enemy;
import com.to2gaming.thelastminer.aimodules.interfaces.AiEffect;
import com.to2gaming.thelastminer.manager.MinerList;
import com.to2gaming.thelastminer.mocks.EnemyMock;
import com.to2gaming.thelastminer.phases.phaseBuilding.BuildingType;
import com.to2gaming.thelastminer.phases.phaseBuilding.BuildingsFactory;
import com.to2gaming.thelastminer.tests.GdxTestRunner;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

@RunWith(GdxTestRunner.class)
public class WizardTowerTest {
    private WizardTower wizardTower;
    private MinerList<Enemy> enemies;
    private Enemy strongestEnemy;
    private Enemy secondStrongestEnemy;

    private final int STARTING_HP = 75;

    @Before
    public void setup() {
        wizardTower = new WizardTower(BuildingsFactory.getPath(BuildingType.WIZARD));

        enemies = new MinerList<>();

        strongestEnemy = new EnemyMock(1.0f, 1.0f, 200);
        secondStrongestEnemy = new EnemyMock(1.0f, 1.0f, 150);

        enemies.add(strongestEnemy);
        enemies.add(new EnemyMock(1.0f, 1.0f, 75));
        enemies.add(new EnemyMock(1.0f, 1.0f, 50));
        enemies.add(secondStrongestEnemy);
    }

    @Test
    public void newWizardTowerShouldHaveCorrectProperties() {
        assertEquals(wizardTower.getHp(), 75);
        assertEquals(wizardTower.getDamageRange(), 110);
        assertEquals(wizardTower.getLevel(), 1);
        assertEquals(wizardTower.getDamagePoints(), 130);
    }

    @Test
    public void wizardTowerPropertiesShouldBeModifiedCorrectlyWhenHit() {
        int HIT = 20;
        int EXPECTED_AFTER_HIT = STARTING_HP - HIT;

        wizardTower.getHit(HIT);
        assertEquals(wizardTower.getHp(), EXPECTED_AFTER_HIT);
        assertTrue(wizardTower.isAlive());
    }

    @Test
    public void cannonShouldBeMarkedAsDeadWhenNoHp() {
        int HIT = 110;
        int EXPECTED_AFTER_HIT = STARTING_HP - HIT;

        wizardTower.getHit(HIT);
        assertEquals(wizardTower.getHp(), EXPECTED_AFTER_HIT);
        assertTrue(!wizardTower.isAlive());
    }

    @Test
    public void wizardTowerShouldSelectFewStrongestEnemiesAndAttackThem() {
        List<AiEffect> attacksToTrigger = wizardTower.behave(enemies);

        for(AiEffect attack : attacksToTrigger) {
            attack.doIt();
        }

        assertEquals(attacksToTrigger.get(0).getTarget(), strongestEnemy);
        assertEquals(strongestEnemy.getHp(), 70);

        assertEquals(attacksToTrigger.get(1).getTarget(), secondStrongestEnemy);
        assertEquals(secondStrongestEnemy.getHp(), 20);
    }

    @Test
    public void wizardTowerShouldAttackRandomEnemyWithHugePower() {
        List<AiEffect> attacksToTrigger = wizardTower.die(enemies);

        for(AiEffect attack : attacksToTrigger) {
            attack.doIt();
        }

        assertEquals(attacksToTrigger.size(), 1);
        assertTrue(!attacksToTrigger.get(0).getTarget().isAlive());
    }
}