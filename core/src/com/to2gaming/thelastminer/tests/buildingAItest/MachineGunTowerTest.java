package com.to2gaming.thelastminer.tests.buildingAItest;


import com.to2gaming.thelastminer.aimodules.enemyai.Enemy;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;

import com.to2gaming.thelastminer.aimodules.buildingsai.Building;
import com.to2gaming.thelastminer.aimodules.buildingsai.buildings.MachineGunTower;
import com.to2gaming.thelastminer.aimodules.buildingsai.buildings.TwoMachineGunsTower;
import com.to2gaming.thelastminer.aimodules.interfaces.AiEffect;
import com.to2gaming.thelastminer.manager.MinerList;
import com.to2gaming.thelastminer.mocks.EnemyMock;
import com.to2gaming.thelastminer.phases.phaseBuilding.BuildingType;
import com.to2gaming.thelastminer.phases.phaseBuilding.BuildingsFactory;
import com.to2gaming.thelastminer.tests.GdxTestRunner;
import org.junit.*;
import org.junit.runner.RunWith;

import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;

@RunWith(GdxTestRunner.class)
public class MachineGunTowerTest {
    private MachineGunTower machineGunTower;
    private MinerList<Enemy> enemies;
    private Enemy weakestEnemy;

    @Before
    public void setup() {
        Gdx.gl = mock(GL20.class);

        machineGunTower = new MachineGunTower(BuildingsFactory.getPath(BuildingType.MACHINE_GUN));

        enemies = new MinerList<>();

        Enemy e1 = new EnemyMock(1.0f, 1.0f, 100);
        Enemy e2 = new EnemyMock(10.0f, 10.0f, 90);
        weakestEnemy = new EnemyMock(20.0f, 20.0f, 80);

        enemies.add(e1);
        enemies.add(e2);
        enemies.add(weakestEnemy);
    }

    @Test
    public void startBasicTest() {
        assertEquals(machineGunTower.getHp(), 20);
        assertEquals(machineGunTower.getDamagePoints(), 10);
        assertEquals(machineGunTower.getDamageRange(), 50);
        assertEquals(machineGunTower.getLevel(), 1);
    }

    @Test
    public void testIfGetsHitProperly() {
        final int SMALL_HIT = 10;
        final int EXPECTED_AFTER_SMALL_HIT = 10;
        machineGunTower.getHit(SMALL_HIT);
        assertEquals(machineGunTower.getHp(), EXPECTED_AFTER_SMALL_HIT);
    }

    @Test
    public void testIfDiesProperly() {
        final int HUGE_HIT = 45;
        final int EXPECTED_AFTER_HUGE_HIT = -25;

        machineGunTower.setCurrentHealthPoints(machineGunTower.getMaxHealthPoints());
        machineGunTower.getHit(HUGE_HIT);
        assertEquals(machineGunTower.getHp(), EXPECTED_AFTER_HUGE_HIT);
        assertTrue(!machineGunTower.isAlive());
    }

    @Test
    public void testIfBuildingNotAlive() {
        machineGunTower.setCurrentHealthPoints(-1);
        assertTrue(!machineGunTower.isAlive());
    }

    @Test
    public void testAiEffectReturnedByBuilding() {
        List<AiEffect> attacksToTrigger = machineGunTower.behave(enemies);

        for(AiEffect attack : attacksToTrigger) {
            attack.doIt();
        }

        assertEquals(attacksToTrigger.get(0).getTarget(), weakestEnemy);
        assertEquals(weakestEnemy.getHp(), 70);
    }

    @Test
    public void testAiEffectAfterBuildingsDeath() {
        List<AiEffect> attacksToTrigger = machineGunTower.die(enemies);

        for(AiEffect attack : attacksToTrigger) {
            attack.doIt();
        }

        assertEquals(attacksToTrigger.get(0).getTarget(), weakestEnemy);
        assertEquals(weakestEnemy.getHp(), 70);
    }
}
