package com.to2gaming.thelastminer.tests.buildingAItest;

import com.to2gaming.thelastminer.aimodules.buildingsai.Building;
import com.to2gaming.thelastminer.aimodules.buildingsai.buildings.CannonTower;
import com.to2gaming.thelastminer.aimodules.enemyai.Enemy;
import com.to2gaming.thelastminer.aimodules.interfaces.AiEffect;
import com.to2gaming.thelastminer.manager.MinerList;
import com.to2gaming.thelastminer.mocks.EnemyMock;
import com.to2gaming.thelastminer.phases.phaseBuilding.BuildingType;
import com.to2gaming.thelastminer.phases.phaseBuilding.BuildingsFactory;
import com.to2gaming.thelastminer.tests.GdxTestRunner;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

@RunWith(GdxTestRunner.class)
public class CannonTowerTest {
    private Building cannonTower;
    private MinerList<Enemy> enemies;
    private Enemy strongestEnemy;

    private final int STARTING_HP = 100;

    @Before
    public void setup() {
        cannonTower = BuildingsFactory.getBuilding(BuildingType.CANNON);

        enemies = new MinerList<>();

        strongestEnemy = new EnemyMock(1.0f, 1.0f, 200);

        enemies.add(strongestEnemy);
        enemies.add(new EnemyMock(1.0f, 1.0f, 100));
        enemies.add(new EnemyMock(1.0f, 1.0f, 150));
        enemies.add(new EnemyMock(1.0f, 1.0f, 70));
    }

    @Test
    public void newCannonTowerShouldHaveCorrectProperties() {
        assertEquals(cannonTower.getHp(), 100);
        assertEquals(cannonTower.getDamagePoints(), 100);
        assertEquals(cannonTower.getDamageRange(), 100);
        assertEquals(cannonTower.getLevel(), 1);
    }

    @Test
    public void cannonTowerPropertiesShouldBeModifiedCorrectlyWhenHit() {
        int HIT = 20;
        int EXPECTED_AFTER_HIT = STARTING_HP - HIT;

        cannonTower.getHit(HIT);
        assertEquals(cannonTower.getHp(), EXPECTED_AFTER_HIT);
        assertTrue(cannonTower.isAlive());
    }

    @Test
    public void cannonTowerShouldBeMarkedAsDeadWhenNoHp() {
        int HIT = 110;
        int EXPECTED_AFTER_HIT = STARTING_HP - HIT;

        cannonTower.getHit(HIT);
        assertEquals(cannonTower.getHp(), EXPECTED_AFTER_HIT);
        assertTrue(!cannonTower.isAlive());
    }

    @Test
    public void cannonTowerShouldSelectStrongestEnemyWithinRange() {
        List<AiEffect> attacksToTrigger = cannonTower.behave(enemies);

        for(AiEffect attack : attacksToTrigger) {
            attack.doIt();
        }

        assertEquals(attacksToTrigger.get(0).getTarget(), strongestEnemy);
        assertEquals(strongestEnemy.getHp(), 100);
    }

    @Test
    public void cannonTowerShouldDoNothingWhenDies() {
        List<AiEffect> attacksToTrigger = cannonTower.die(enemies);
        assertEquals(attacksToTrigger.size(), 0);
    }
}