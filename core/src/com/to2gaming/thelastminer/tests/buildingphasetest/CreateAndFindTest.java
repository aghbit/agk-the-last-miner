package com.to2gaming.thelastminer.tests.buildingphasetest;

import com.to2gaming.thelastminer.aimodules.buildingsai.Building;
import com.to2gaming.thelastminer.phases.phaseBuilding.*;
import com.to2gaming.thelastminer.manager.MinerList;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

/**
 * @author Mateusz Jarosz (mateusja)
 * @author Grzegorz Łaganowski (laganows)
 */


public class CreateAndFindTest {
    private BuildingPhase bp;

    @Before
    public void setup() {
        bp = new BuildingPhase();
        bp.setBuildings(new MinerList<Building>());
        bp.activate();

        try {
            bp.loadProperties();
        } catch (BuildException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void createAndFind() {
        bp.setCash(Integer.MAX_VALUE);
        assertTrue(bp.build(BuildingType.MACHINE_GUN, 12, 12) == BuildReturn.SUCCESS);
        assertFalse(bp.isPlaceToBuild(12, 12));
        assertTrue(bp.isPlaceToBuild(252, 252));
    }

    @Test
    public void createAndOverlap() throws BuildException {
        bp.setCash(Integer.MAX_VALUE);
        assertTrue(bp.build(BuildingType.MACHINE_GUN, 12, 12) == BuildReturn.SUCCESS);
        assertTrue(bp.build(BuildingType.MACHINE_GUN, 13, 13) == BuildReturn.ALREADY_TAKEN);
    }


    @Test
    public void createWithNoGold() throws BuildException {
        bp.setCash(0);
        assertTrue(bp.build(BuildingType.MACHINE_GUN, 12, 12) == BuildReturn.NO_GOLD);
    }

}
