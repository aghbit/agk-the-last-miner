package com.to2gaming.thelastminer.tests.buildingphasetest;

import com.to2gaming.thelastminer.aimodules.buildingsai.Building;
import com.to2gaming.thelastminer.phases.phaseBuilding.*;
import com.to2gaming.thelastminer.manager.MinerList;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertTrue;

/**
 * @author Mateusz Jarosz (mateusja)
 * @author Grzegorz Łaganowski (laganows)
 */


public class UpgradeTest {
    private BuildingPhase bp;

    @Before
    public void setup() {
        bp = new BuildingPhase();
        bp.setBuildings(new MinerList<Building>());
        bp.activate();

        try {
            bp.loadProperties();
        } catch (BuildException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void upgradeNonexistentBuilding() throws BuildException {
        assertTrue(bp.upgradeBuilding(100, 100) == BuildReturn.NO_BUILDING);

    }

    @Test
    public void upgradeWithoutMoney() throws BuildException {
        bp.setCash(Integer.MAX_VALUE);
        assertTrue(bp.build(BuildingType.MACHINE_GUN, 12, 12) == BuildReturn.SUCCESS);
        bp.setCash(0);
        assertTrue(bp.upgradeBuilding(12, 12) == BuildReturn.NO_GOLD);
    }

    @Test
    public void createAndUpgrade() {
        bp.setCash(Integer.MAX_VALUE);
        assertTrue(bp.build(BuildingType.MACHINE_GUN, 12, 12) == BuildReturn.SUCCESS);

        bp.setCash(Integer.MAX_VALUE);
        assertTrue(bp.upgradeBuilding(13, 13) == BuildReturn.SUCCESS);
    }

}
