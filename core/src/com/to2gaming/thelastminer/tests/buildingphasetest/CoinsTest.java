package com.to2gaming.thelastminer.tests.buildingphasetest;


import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.to2gaming.thelastminer.TheLastMinerGame;
import com.to2gaming.thelastminer.aimodules.buildingsai.Building;
import com.to2gaming.thelastminer.phases.phaseBuilding.BuildingPhase;
import com.to2gaming.thelastminer.phases.phaseBuilding.CoinEffect;
import com.to2gaming.thelastminer.manager.MinerList;
import com.to2gaming.thelastminer.tests.GdxTestRunner;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;

/**
 * @author Mateusz Jarosz (mateusja)
 * @author Grzegorz Łaganowski (laganows)
 */
@RunWith(GdxTestRunner.class)

public class CoinsTest {
    private BuildingPhase bp;

    @Before
    public void setup() {
        Gdx.gl = mock(GL20.class);
        bp = new BuildingPhase(new TheLastMinerGame());
        bp.setBuildings(new MinerList<Building>());
        bp.activate();
        bp.setCash(0);
    }

    @Test
    public void clickCoin() {
        assertFalse(bp.getCoins().isEmpty());
        CoinEffect coin = (CoinEffect) bp.getCoins().get(0);
        bp.clickCoin(coin.getX(),coin.getY());
        assertTrue(bp.getCash() != 0);
    }


}
