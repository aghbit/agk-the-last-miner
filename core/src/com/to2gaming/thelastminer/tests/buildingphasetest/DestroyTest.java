package com.to2gaming.thelastminer.tests.buildingphasetest;

import com.to2gaming.thelastminer.aimodules.buildingsai.Building;
import com.to2gaming.thelastminer.phases.phaseBuilding.*;
import com.to2gaming.thelastminer.manager.MinerList;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * @author Mateusz Jarosz (mateusja)
 * @author Grzegorz Łaganowski (laganows)
 */


public class DestroyTest {
    private BuildingPhase bp;

    @Before
    public void setup() {
        bp = new BuildingPhase();
        bp.setBuildings(new MinerList<Building>());
        bp.activate();
        bp.setCash(Integer.MAX_VALUE);
        try {
            bp.loadProperties();
        } catch (BuildException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void destroyNonexistentBuilding() throws BuildException {
        assertTrue(bp.destroyBuilding(100, 100) == BuildReturn.NO_BUILDING);
    }

    @Test
    public void createAndDestroy() {
        assertTrue(bp.build(BuildingType.MACHINE_GUN, 12, 12) == BuildReturn.SUCCESS);
        assertTrue(bp.destroyBuilding(14, 12) == BuildReturn.SUCCESS);

        assertEquals(bp.getBuildings().isEmpty(), true);
        assertTrue(bp.getGrid().deleteFromGrid(14, 14) == BuildReturn.NO_BUILDING);
    }

}
