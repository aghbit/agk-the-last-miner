package com.to2gaming.thelastminer.tests.buildingphasetest;

import com.to2gaming.thelastminer.phases.phaseBuilding.BuildException;
import com.to2gaming.thelastminer.phases.phaseBuilding.BuildingPhase;
import com.to2gaming.thelastminer.phases.phaseBuilding.BuildingType;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertTrue;

/**
 * @author Mateusz Jarosz (mateusja)
 * @author Grzegorz Łaganowski (laganows)
 */

public class InitializePropertiesTest {
    private BuildingPhase bp;

    @Before
    public void setup() {
        bp = new BuildingPhase();
        try {
            bp.loadProperties();
        } catch (BuildException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void buildingsNumber() {
        assertTrue(bp.getBuildingTypesNumber() > 0);
    }

    @Test
    public void notEmptyCostMap() {
        assertTrue(bp.getBuildingsCosts().size() > 0);
    }

    @Test
    public void normalTowerPrices() {
        assertTrue(bp.getBuildingsCosts().get(BuildingType.MACHINE_GUN) > 0);
    }
}