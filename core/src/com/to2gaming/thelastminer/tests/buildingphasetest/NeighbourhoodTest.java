package com.to2gaming.thelastminer.tests.buildingphasetest;

import com.to2gaming.thelastminer.aimodules.buildingsai.Building;
import com.to2gaming.thelastminer.phases.phaseBuilding.*;
import com.to2gaming.thelastminer.manager.MinerList;
import org.junit.Before;
import org.junit.Test;

import static junit.framework.TestCase.assertFalse;
import static org.junit.Assert.assertTrue;

/**
 * @author Mateusz Jarosz (mateusja)
 * @author Grzegorz Łaganowski (laganows)
 */

public class NeighbourhoodTest {
    private BuildingPhase bp;
    private float boardHeight;
    private float boardWidth;
    private float cellHeight;
    private float cellWidth;

    @Before
    public void setup() {
        bp = new BuildingPhase();
        bp.setBuildings(new MinerList<Building>());
        bp.activate();

        try {
            bp.loadProperties();
        } catch (BuildException e) {
            e.printStackTrace();
        }
        boardHeight = bp.getGrid().getBoardHeight();
        cellHeight = bp.getGrid().getCellHeight();
        boardWidth = bp.getGrid().getBoardWidth();
        cellWidth = bp.getGrid().getCellWidth();
    }

    @Test
    public void lowerUpgradeCostInNeighbourhood() {
        bp.setCash(Integer.MAX_VALUE);
        assertTrue(bp.build(BuildingType.MACHINE_GUN, 6, 8) == BuildReturn.SUCCESS);
        assertTrue(bp.build(BuildingType.MACHINE_GUN, 7, 0) == BuildReturn.SUCCESS);
        assertTrue(0.75 * bp.getBuildings().get(0).getUpgradeMul() == bp.getBuildings().get(1).getUpgradeMul());
        assertTrue(bp.getGrid().checkNeighbourhood(BuildingType.MACHINE_GUN,6, 8));
    }

    @Test
    public void leftDownCorner() {
        bp.setCash(Integer.MAX_VALUE);
        assertTrue(bp.build(BuildingType.MACHINE_GUN, 0, boardHeight - cellHeight - 1) == BuildReturn.SUCCESS);
        assertTrue(bp.build(BuildingType.MACHINE_GUN, 0, boardHeight - 1) == BuildReturn.SUCCESS);
        assertTrue(bp.getGrid().checkNeighbourhood(BuildingType.MACHINE_GUN, 0, boardHeight - cellHeight - 1));
    }

    @Test
    public void leftUpCorner() {
        bp.setCash(Integer.MAX_VALUE);
        assertTrue(bp.build(BuildingType.MACHINE_GUN, 0, 0) == BuildReturn.SUCCESS);
        assertTrue(bp.build(BuildingType.MACHINE_GUN, 0, cellHeight + 1) == BuildReturn.SUCCESS);
        assertTrue(bp.getGrid().checkNeighbourhood(BuildingType.MACHINE_GUN, 0, 0));
    }

    @Test
    public void rightDownCorner() {
        bp.setCash(Integer.MAX_VALUE);
        assertTrue(bp.build(BuildingType.MACHINE_GUN, boardWidth - 1, boardHeight - cellHeight - 1) == BuildReturn.SUCCESS);
        assertTrue(bp.build(BuildingType.MACHINE_GUN, boardWidth - 1, boardHeight - 1) == BuildReturn.SUCCESS);
        assertTrue(bp.getGrid().checkNeighbourhood(BuildingType.MACHINE_GUN, boardWidth - 1, boardHeight - cellHeight - 1));
    }

    @Test
    public void rightUpCorner() {
        bp.setCash(Integer.MAX_VALUE);
        assertTrue(bp.build(BuildingType.MACHINE_GUN, 0, boardHeight - cellHeight - 1) == BuildReturn.SUCCESS);
        assertTrue(bp.build(BuildingType.MACHINE_GUN, 0, boardHeight - 1) == BuildReturn.SUCCESS);
        assertTrue(bp.getGrid().checkNeighbourhood(BuildingType.MACHINE_GUN, 0, boardHeight - cellHeight - 1));
    }

    @Test
    public void center() {
        bp.setCash(Integer.MAX_VALUE);
        assertTrue(bp.build(BuildingType.MACHINE_GUN, 2 * cellWidth, 2 * cellHeight - 1) == BuildReturn.SUCCESS);
        assertTrue(bp.build(BuildingType.MACHINE_GUN, 2 * cellWidth, 3 * cellHeight - 1) == BuildReturn.SUCCESS);
        assertTrue(bp.getGrid().checkNeighbourhood(BuildingType.MACHINE_GUN, 2 * cellWidth, 2 * cellHeight - 1));
    }

    @Test
    public void upLine() {
        bp.setCash(Integer.MAX_VALUE);
        assertTrue(bp.build(BuildingType.MACHINE_GUN, 2 * cellWidth - 1, 0) == BuildReturn.SUCCESS);
        assertTrue(bp.build(BuildingType.MACHINE_GUN, 3 * cellWidth - 1, 0) == BuildReturn.SUCCESS);
        assertTrue(bp.getGrid().checkNeighbourhood(BuildingType.MACHINE_GUN, 2 * cellWidth - 1, 0));
    }

    @Test
    public void downLine() {
        bp.setCash(Integer.MAX_VALUE);
        assertTrue(bp.build(BuildingType.MACHINE_GUN, 2 * cellWidth - 1, boardHeight - 1) == BuildReturn.SUCCESS);
        assertTrue(bp.build(BuildingType.MACHINE_GUN, 3 * cellWidth - 1, boardHeight - 1) == BuildReturn.SUCCESS);
        assertTrue(bp.getGrid().checkNeighbourhood(BuildingType.MACHINE_GUN, 2 * cellWidth - 1, boardHeight - 1));
    }

    @Test
    public void rightLine() {
        bp.setCash(Integer.MAX_VALUE);
        assertTrue(bp.build(BuildingType.MACHINE_GUN, boardWidth - 1, 2 * cellHeight - 1) == BuildReturn.SUCCESS);
        assertTrue(bp.build(BuildingType.MACHINE_GUN, boardWidth - 1, 3 * cellHeight - 1) == BuildReturn.SUCCESS);
        assertTrue(bp.getGrid().checkNeighbourhood(BuildingType.MACHINE_GUN, boardWidth - 1, 2 * cellHeight - 1));
    }

    @Test
    public void leftLine() {
        bp.setCash(Integer.MAX_VALUE);
        assertTrue(bp.build(BuildingType.MACHINE_GUN, 0, 2 * cellHeight - 1) == BuildReturn.SUCCESS);
        assertTrue(bp.build(BuildingType.MACHINE_GUN, 0, 3 * cellHeight - 1) == BuildReturn.SUCCESS);
        assertTrue(bp.getGrid().checkNeighbourhood(BuildingType.MACHINE_GUN, 0, 2 * cellHeight - 1));
    }

    @Test
    public void noNeighbour() {
        bp.setCash(Integer.MAX_VALUE);
        assertTrue(bp.build(BuildingType.MACHINE_GUN, 5 * cellWidth - 1, 5 * cellHeight - 1) == BuildReturn.SUCCESS);
        assertFalse(bp.getGrid().checkNeighbourhood(BuildingType.MACHINE_GUN, 5 * cellWidth - 1, 5 * cellHeight - 1));
    }
}
