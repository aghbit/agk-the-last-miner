package com.to2gaming.thelastminer.tests.managertest;

import com.to2gaming.thelastminer.TheLastMinerGame;
import com.to2gaming.thelastminer.manager.Difficulty;
import com.to2gaming.thelastminer.manager.GamePhase;
import com.to2gaming.thelastminer.phases.phaseDefence.DefencePhase;
import org.junit.Before;
import org.junit.Test;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

/**
 * Created by haz111 on 21.11.15.
 */
public class TheLastMinerGameTest {

    Class cls;
    Field gamePhase;
    Field gameData;
    Field ui;
    Field buildingPhase;
    Field fightingPhase;
    Field startGameFlag;
    Field startFightingSceneFlag;
    Field startBuildingSceneFlag;
    Method startPhaseMethod;
    Method buildingPhaseMethod;
    Method fightingPhaseMethod;

    TheLastMinerGame tlmg;

    @Before
    public void before() throws ClassNotFoundException, NoSuchFieldException, NoSuchMethodException {
        cls = Class.forName("com.to2gaming.thelastminer.TheLastMinerGame");

        gamePhase = cls.getDeclaredField("gamePhase");
        gamePhase.setAccessible(true);
        gameData = cls.getDeclaredField("gameData");
        gameData.setAccessible(true);
        ui = cls.getDeclaredField("ui");
        ui.setAccessible(true);
        buildingPhase = cls.getDeclaredField("buildingPhaseModule");
        buildingPhase.setAccessible(true);
        fightingPhase = cls.getDeclaredField("fightingPhaseModule");
        fightingPhase.setAccessible(true);
        startGameFlag = cls.getDeclaredField("startGameFlag");
        startGameFlag.setAccessible(true);
        startFightingSceneFlag = cls.getDeclaredField("startFightingSceneFlag");
        startFightingSceneFlag.setAccessible(true);
        startBuildingSceneFlag = cls.getDeclaredField("startBuildingSceneFlag");
        startBuildingSceneFlag.setAccessible(true);
        startPhaseMethod = cls.getDeclaredMethod("startPhase");
        startPhaseMethod.setAccessible(true);
        buildingPhaseMethod = cls.getDeclaredMethod("buildingPhase", float.class);
        buildingPhaseMethod.setAccessible(true);
        fightingPhaseMethod = cls.getDeclaredMethod("fightingPhase", float.class);
        fightingPhaseMethod.setAccessible(true);
        tlmg = new TheLastMinerGame();
        tlmg.create();
    }

    @Test
    public void testCreateGamePhaseIsStart() throws Exception {
        // Arrange
        GamePhase gamePhaseCheck;
        // Act
        gamePhaseCheck = (GamePhase) gamePhase.get(tlmg);
        // Assert
        assertThat(gamePhaseCheck, is(equalTo(GamePhase.START)));
    }

    @Test
    public void testCreateCheckIfBuilingPhase() throws Exception {
        assertNotNull(buildingPhase);
    }

    @Test
    public void testUpdate() throws Exception {

    }

    @Test
    public void testRender() throws Exception {

    }

    @Test
    public void testStartGameIfPhaseWasStart() throws Exception {
        // Arrange
        gamePhase.set(tlmg, GamePhase.START);
        startGameFlag.setBoolean(tlmg, false);
        // Act
        tlmg.startGame("Hubert", Difficulty.EASY);
        // Assert
        assertTrue(startGameFlag.getBoolean(tlmg));
    }

    @Test
    public void testStartGameIfPhaseWasNotStart() throws Exception {
        // Arrange
        gamePhase.set(tlmg, GamePhase.BUILDING);
        startGameFlag.setBoolean(tlmg, false);
        // Act
        tlmg.startGame("Hubert", Difficulty.EASY);
        // Assert
        assertFalse(startGameFlag.getBoolean(tlmg));
    }

    @Test
    public void testEndBuilidingPhaseIfGameWasInBuildingPhase() throws Exception {
        // Arrange
        gamePhase.set(tlmg, GamePhase.BUILDING);
        startFightingSceneFlag.setBoolean(tlmg, false);
        // Act
        tlmg.endBuildingPhase();
        // Assert
        assertTrue(startFightingSceneFlag.getBoolean(tlmg));
    }

    @Test
    public void testEndBuilidingPhaseIfGameWasInOtherPhase() throws Exception {
        // Arrange
        gamePhase.set(tlmg, GamePhase.FIGHTING);
        startFightingSceneFlag.setBoolean(tlmg, false);
        // Act
        tlmg.endBuildingPhase();
        // Assert
        assertFalse(startFightingSceneFlag.getBoolean(tlmg));
    }

    @Test
    public void testEndFightingPhaseIfGameWasInFightingPhase() throws Exception {
        // Arrange
        gamePhase.set(tlmg, GamePhase.FIGHTING);
        startFightingSceneFlag.setBoolean(tlmg, false);
        // Act
        tlmg.endFightingPhase();
        // Assert
        assertTrue(startBuildingSceneFlag.getBoolean(tlmg));
    }

    @Test
    public void testEndFightingPhaseIfGameWasInOtherPhase() throws Exception {
        // Arrange
        gamePhase.set(tlmg, GamePhase.BUILDING);
        startFightingSceneFlag.setBoolean(tlmg, false);
        // Act
        tlmg.endFightingPhase();
        // Assert
        assertFalse(startBuildingSceneFlag.getBoolean(tlmg));
    }

    @Test
    public void testStartPhaseIfFlagWasTrue() throws IllegalAccessException, InvocationTargetException {
        // Arrange
        startGameFlag.setBoolean(tlmg, true);
        // Act
        startPhaseMethod.invoke(tlmg, null);
        // Assert
        assertFalse(startGameFlag.getBoolean(tlmg));

    }

    @Test
    public void testStartPhaseIfFlagWasFalse() throws IllegalAccessException, InvocationTargetException {
        // Arrange
        startGameFlag.setBoolean(tlmg, true);
        // Act
        startPhaseMethod.invoke(tlmg, null);
        // Assert
        assertFalse(startGameFlag.getBoolean(tlmg));
    }

    @Test
    public void testBuildingPhaseIfStartFightingSceneFlagWasTrue() throws IllegalAccessException, InvocationTargetException {
        // Arrange
        startFightingSceneFlag.setBoolean(tlmg, true);
        gamePhase.set(tlmg, GamePhase.BUILDING);
        // Act
        buildingPhaseMethod.invoke(tlmg, 0f);
        // Assert
        GamePhase gamePhaseCheck = (GamePhase) gamePhase.get(tlmg);
        assertFalse(startFightingSceneFlag.getBoolean(tlmg));
        assertThat(gamePhaseCheck, equalTo(GamePhase.FIGHTING));
    }

    @Test
    public void testBuildingPhaseIfStartFightingSceneFlagWasFalse() throws IllegalAccessException, InvocationTargetException {
        // Arrange
        startFightingSceneFlag.setBoolean(tlmg, false);
        gamePhase.set(tlmg, GamePhase.BUILDING);
        // Act
        buildingPhaseMethod.invoke(tlmg, 0f);
        // Assert
        GamePhase gamePhaseCheck = (GamePhase) gamePhase.get(tlmg);
        assertFalse(startFightingSceneFlag.getBoolean(tlmg));
        assertThat(gamePhaseCheck, equalTo(GamePhase.BUILDING));
    }

    @Test
    public void testFightingPhaseIfStartBuildingSceneFlagWasSetAndResultWasTrue() throws InvocationTargetException, IllegalAccessException {
        // Arrange
        DefencePhase mockedDefencePhaseModule = mock(DefencePhase.class);
        when(mockedDefencePhaseModule.getResult()).thenReturn(true);
        fightingPhase.set(tlmg, mockedDefencePhaseModule);
        gamePhase.set(tlmg, GamePhase.FIGHTING);
        startBuildingSceneFlag.set(tlmg, true);
        // Act
        fightingPhaseMethod.invoke(tlmg, 0f);
        // Assert
        GamePhase gamePhaseCheck = (GamePhase) gamePhase.get(tlmg);
        assertEquals(GamePhase.BUILDING, gamePhaseCheck);
    }

    @Test(expected=InvocationTargetException.class)
    public void testFightingPhaseIfStartBuildingSceneFlagWasSetAndResultWasFalse() throws InvocationTargetException, IllegalAccessException {
        // Arrange
        DefencePhase mockedDefencePhaseModule = mock(DefencePhase.class);
        fightingPhase.set(tlmg, mockedDefencePhaseModule);
        when(mockedDefencePhaseModule.getResult()).thenReturn(false);
        startBuildingSceneFlag.set(tlmg, true);
        // Act
        fightingPhaseMethod.invoke(tlmg, 0f);
        // Assert

    }
}