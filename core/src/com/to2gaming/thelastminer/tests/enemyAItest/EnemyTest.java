package com.to2gaming.thelastminer.tests.enemyAItest;

import com.to2gaming.thelastminer.aimodules.enemyai.Enemy;
import com.to2gaming.thelastminer.aimodules.enemyai.enemies.BasicAirEnemy;
import com.to2gaming.thelastminer.aimodules.enemyai.enemies.BasicGroundEnemy;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * Created by Kuba on 28.11.2015.
 */
public class EnemyTest {
    Enemy groundEnemy;
    Enemy airEnemy;

    @Before
    public void setup() {

        groundEnemy = new BasicGroundEnemy(0, 0);
        airEnemy = new BasicAirEnemy(0, 0);

    }

    @Test
    public void basicTests(){
        assertEquals(5,groundEnemy.getDmg());
        assertEquals(20,groundEnemy.getHp());
        assertEquals(5,groundEnemy.getWidth());
        assertEquals(5,groundEnemy.getHeight());
        assertEquals("BasicGround",groundEnemy.getName());
        assertTrue(groundEnemy.isAlive());

        assertEquals(5,airEnemy.getDmg());
        assertEquals(20,airEnemy.getHp());
        assertEquals(5,airEnemy.getWidth());
        assertEquals(5,airEnemy.getHeight());
        assertEquals("BasicAir",airEnemy.getName());
        assertTrue(airEnemy.isAlive());
    }


    @Test
    public void testPerformAttack() {

    }

    @Test
    public void testPeformMove(){
//        airEnemy.move();
//        assertEquals(airEnemy.getX(),1);
//        assertEquals(airEnemy.getY(),1);
//
//        groundEnemy.move();
//        assertEquals(groundEnemy.getX(),1);
//        assertEquals(groundEnemy.getY(),1);
    }

    @Test
    public void testPeformDie() {

    }



}
