package com.to2gaming.thelastminer.aimodules.interfaces;

/**
 * Created by Konrad on 2015-12-04.
 */
public interface AiEffect {
    void doIt();
    ObjectAI getTarget();
}
