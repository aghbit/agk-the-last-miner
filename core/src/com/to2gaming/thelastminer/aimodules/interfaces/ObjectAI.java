package com.to2gaming.thelastminer.aimodules.interfaces;

import com.to2gaming.thelastminer.manager.MinerList;

import java.util.List;

public interface ObjectAI {
    boolean getHit(Integer dmg);
    List<AiEffect> die(MinerList<? extends GameObject> objects);
    List<AiEffect> behave(MinerList<? extends GameObject> objects);
    int getHp();
    float getX();
    float getY();
    int getDamageRange();
    boolean isAlive();
}
