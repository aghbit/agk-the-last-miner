package com.to2gaming.thelastminer.aimodules.buildingsai;

import com.to2gaming.thelastminer.aimodules.enemyai.Enemy;
import com.to2gaming.thelastminer.aimodules.interfaces.AiEffect;
import com.to2gaming.thelastminer.aimodules.interfaces.GameObject;
import com.to2gaming.thelastminer.manager.MinerList;
import com.to2gaming.thelastminer.manager.comparators.ComparatorByHealth;
import com.to2gaming.thelastminer.phases.phaseBuilding.BuildingType;
import com.to2gaming.thelastminer.phases.phaseDefence.Attack;
import com.to2gaming.thelastminer.phases.phaseDefence.AttackType;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

public abstract class Building extends GameObject {
    private int maxHealthPoints;
    private int currentHealthPoints;
    private int level;
    private int damageRange;
    private int damagePoints;
    private int damageWhenDies = 0;
    private String imagePath;
    private float timer = 0f;
    private float delay = 0f;
    private AttackType attackType; //TODO: Where and who sets type of attack?
    private int cost;
    private BuildingType name;
    private double upgradeMul = 2;

    public void update(float deltaTime) {
        this.addTime(deltaTime);
    }

    @Override
    public boolean getHit(Integer dmg) {
        this.setCurrentHealthPoints(this.getHp() - dmg);
        return this.isAlive();
    }

    public Building(String pathToImage) {
        setImageAsActor(pathToImage);
        imagePath = pathToImage;
    }


    public boolean isAlive() {
        return currentHealthPoints > 0;
    }

    public int getMaxHealthPoints() {
        return maxHealthPoints;
    }

    public void setMaxHealthPoints(int maxHealthPoints) {
        this.maxHealthPoints = maxHealthPoints;
    }

    public void setCost(int cost) {
        this.cost = cost;
    }

    public int getCost() {
        return cost;
    }

    public void setName(BuildingType name) {
        this.name = name;
    }

    public BuildingType getBuildingName() {
        return name;
    }

    public void setCurrentHealthPoints(int currentHealthPoints) {
        this.currentHealthPoints = currentHealthPoints;
    }

    @Override
    public int getHp() {
        return this.currentHealthPoints;
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public int getDamageRange() {
        return damageRange;
    }

    public void setDamageRange(int range) {
        this.damageRange = range;
    }

    public int getDamagePoints() {
        return damagePoints;
    }

    public void setDamagePoints(int damagePoints) {
        this.damagePoints = damagePoints;
    }

    public String getImagePath() {
        return imagePath;
    }

    public void setImagePath(String imagePath) {
        this.imagePath = imagePath;
    }

    public boolean isReady() {
        return timer >= delay;
    }

    public void addTime(float time) {
        this.timer += time;
    }

    public void setTime(float time) {
        this.timer = time;
    }

    public void setDelay(float time) {
        this.delay = time;
    }

    protected List<AiEffect> attackAll(MinerList<? extends GameObject> enemies) {
        this.setTime(0);
        List<AiEffect> attacks = new ArrayList<>();

        for(GameObject e : enemies) {
            attacks.add(new Attack(e, attackType, getDamagePoints()));
        }
        return attacks;
    }

    protected List<AiEffect> attackRandomEnemy(MinerList<? extends GameObject> enemiesInRange) {
        this.setTime(0);
        int min = 0;
        int max = enemiesInRange.size();
        Random random = new Random();
        int randomEnemy = random.nextInt((max - min)) + min;

        List<AiEffect> attacksToTrigger = new ArrayList<>();
        attacksToTrigger.add(new Attack(enemiesInRange.get(randomEnemy), AttackType.FIRE, this.getDamageWhenDies()));

        return attacksToTrigger;
    }

    protected List<AiEffect> attackWeakestEnemy(MinerList<? extends  GameObject> enemies) {
        this.setTime(0);

        List<AiEffect> attacksToTrigger = new ArrayList<>();

        if (!enemies.isEmpty()) {
            GameObject target = enemies.get(0);
            for(GameObject enemy : enemies) {
                if(enemy.getHp() < target.getHp()) {
                    target = enemy;
                }
            }

            attacksToTrigger.add(new Attack(target, AttackType.IRON, this.getDamagePoints()));

        }

        return attacksToTrigger;
    }

    protected List<AiEffect> attackStrongestEnemy(MinerList<? extends  GameObject> enemies) {
        this.setTime(0);

        List<AiEffect> attacksToTrigger = new ArrayList<>();

        if (!enemies.isEmpty()) {
            GameObject target = enemies.get(0);
            for(GameObject enemy : enemies) {
                if(enemy.getHp() > target.getHp()) {
                    target = enemy;
                }
            }

            attacksToTrigger.add(new Attack(target, AttackType.IRON, this.getDamagePoints()));

        }

        return attacksToTrigger;
    }

    protected List<AiEffect> attackStrongestEnemies(MinerList<? extends GameObject> enemiesInRange, int focusableOn, AttackType attackType) {
        List<AiEffect> attacksToTrigger = new ArrayList<>();

        Collections.sort(enemiesInRange, new ComparatorByHealth());

        int alreadyAdded = 0;

        for(GameObject enemy : enemiesInRange) {
            attacksToTrigger.add(new Attack(enemy, attackType, this.getDamagePoints()));

            alreadyAdded++;
            if(alreadyAdded == focusableOn) break;
        }

        return attacksToTrigger;
    }

    public float getX() {
        return getActor().getX();
    }

    public void setX(float x) {
        setPosition(x, getActor().getY());
    }

    public float getY() {
        return getActor().getY();
    }

    public void setY(float y) {
        setPosition(getActor().getX(), y);
    }

    public double getUpgradeMul() {
        return upgradeMul;
    }

    public void setUpgradeMul(double upgradeMul) {
        this.upgradeMul = upgradeMul;
    }

    public int getDamageWhenDies() {
        return damageWhenDies;
    }

    public void setDamageWhenDies(int damageWhenDies) {
        this.damageWhenDies = damageWhenDies;
    }

}
