package com.to2gaming.thelastminer.aimodules.buildingsai.buildings;

import com.to2gaming.thelastminer.aimodules.buildingsai.Building;
import com.to2gaming.thelastminer.aimodules.interfaces.AiEffect;
import com.to2gaming.thelastminer.aimodules.interfaces.GameObject;
import com.to2gaming.thelastminer.manager.MinerList;
import com.to2gaming.thelastminer.phases.phaseBuilding.BuildingType;
import com.to2gaming.thelastminer.phases.phaseDefence.Attack;
import com.to2gaming.thelastminer.phases.phaseDefence.AttackType;

import java.util.ArrayList;
import java.util.List;

/**
 * Attacks strongest enemy within range.
 * Huge damage when attacking, no damage when dying, small range.
 */

public class CannonTower extends Building {
    private final int DMG_WHEN_DIES = 0;
    private final int HEALTH = 100;
    private final int DMG = 100;
    private final int RANGE = 100;
    private final float DELAY = 1.5f;

    public CannonTower(String imgPath) {
        super(imgPath);
        this.setName(BuildingType.CANNON);
        this.setDamageRange(this.RANGE);
        this.setMaxHealthPoints(this.HEALTH);
        this.setCurrentHealthPoints(this.HEALTH);
        this.setLevel(1);
        this.setDamagePoints(this.DMG);
        this.setDelay(DELAY);
    }

    @Override
    public List<AiEffect> die(MinerList<? extends GameObject> enemiesInRange) {
        return new ArrayList<>(); //still better than null
    }

    @Override
    public List<AiEffect> behave(MinerList<? extends GameObject> enemiesInRange) {
        return attackStrongestEnemy(enemiesInRange);
    }
}
