package com.to2gaming.thelastminer.aimodules.buildingsai.buildings;

import com.to2gaming.thelastminer.aimodules.buildingsai.Building;
import com.to2gaming.thelastminer.aimodules.interfaces.AiEffect;
import com.to2gaming.thelastminer.aimodules.interfaces.GameObject;
import com.to2gaming.thelastminer.manager.MinerList;
import com.to2gaming.thelastminer.phases.phaseBuilding.BuildingType;
import com.to2gaming.thelastminer.phases.phaseDefence.Attack;
import com.to2gaming.thelastminer.phases.phaseDefence.AttackType;

import java.util.ArrayList;
import java.util.List;

/**
 * Attacks weakest enemy within range.
 */

public class MachineGunTower extends Building {
    private final int DMG_WHEN_DIES = 5;
    private final int HEALTH = 20;
    private final int DMG = 10;
    private final int RANGE = 50;
    private final float DELAY = 0.25f;

    public MachineGunTower(String imgPath) {
        super(imgPath);
        this.setName(BuildingType.MACHINE_GUN);
        this.setDamageRange(this.RANGE);
        this.setMaxHealthPoints(this.HEALTH);
        this.setCurrentHealthPoints(this.HEALTH);
        this.setDamageWhenDies(DMG_WHEN_DIES);
        this.setDelay(DELAY);
        this.setLevel(1);
        this.setDamagePoints(this.DMG);
    }


    @Override
    public List<AiEffect> die(MinerList<? extends GameObject> enemiesInRange) {
        return attackWeakestEnemy(enemiesInRange);
    }

    @Override
    public List<AiEffect> behave(MinerList<? extends GameObject> enemiesInRange) {
        return attackWeakestEnemy(enemiesInRange);
    }

}
