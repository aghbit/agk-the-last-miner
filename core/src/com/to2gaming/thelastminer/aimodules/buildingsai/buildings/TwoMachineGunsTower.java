package com.to2gaming.thelastminer.aimodules.buildingsai.buildings;

import com.to2gaming.thelastminer.aimodules.buildingsai.Building;
import com.to2gaming.thelastminer.aimodules.interfaces.AiEffect;
import com.to2gaming.thelastminer.aimodules.interfaces.GameObject;

import com.to2gaming.thelastminer.manager.MinerList;
import com.to2gaming.thelastminer.phases.phaseBuilding.BuildingType;

import java.util.List;

/**
 * Created by Konrad on 2015-12-05.
 */
public class TwoMachineGunsTower extends Building {
    private final int HEALTH = 70;
    private final int DMG = 30;
    private final int RANGE = 50;
    private final float DELAY = 0.25f;

    public TwoMachineGunsTower(String imgPath) {
        super(imgPath);
        this.setName(BuildingType.TWO_MACHINE_GUN);
        this.setDamageRange(this.RANGE);
        this.setMaxHealthPoints(this.HEALTH);
        this.setCurrentHealthPoints(this.HEALTH);
        this.setDelay(DELAY);
        this.setLevel(1);
        this.setDamagePoints(this.DMG);
    }

    @Override
    public List<AiEffect> behave(MinerList<? extends GameObject> enemiesInRange) {
        return attackWeakestEnemy(enemiesInRange);
    }

    @Override
    public List<AiEffect> die(MinerList<? extends GameObject> objects) {
        return attackWeakestEnemy(objects);
    }

}
