package com.to2gaming.thelastminer.aimodules.buildingsai.buildings;

import com.to2gaming.thelastminer.aimodules.buildingsai.Building;
import com.to2gaming.thelastminer.aimodules.interfaces.AiEffect;
import com.to2gaming.thelastminer.aimodules.interfaces.GameObject;
import com.to2gaming.thelastminer.manager.MinerList;
import com.to2gaming.thelastminer.manager.comparators.ComparatorByHealth;
import com.to2gaming.thelastminer.phases.phaseBuilding.BuildingType;
import com.to2gaming.thelastminer.phases.phaseDefence.Attack;
import com.to2gaming.thelastminer.phases.phaseDefence.AttackType;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

/**
 * Attacks multiple enemies at once. Average damage, average health, huge damage on random enemy when dies.
 */

public class WizardTower extends Building {
    private final int FOCUSABLE_ON = 3;
    private final int DMG_WHEN_DIES = 1000;
    private final int HEALTH = 75;
    private final int DMG = 130;
    private final int RANGE = 110;
    private final float DELAY = 2f;

    public WizardTower(String imgPath) {
        super(imgPath);
        this.setName(BuildingType.WIZARD);
        this.setCurrentHealthPoints(HEALTH);
        this.setMaxHealthPoints(HEALTH);
        this.setDamagePoints(DMG);
        this.setDamageRange(RANGE);
        this.setDamageWhenDies(DMG_WHEN_DIES);
        this.setLevel(1);
        this.setDelay(DELAY);
    }

    @Override
    public List<AiEffect> die(MinerList<? extends GameObject> enemiesInRange) {
        return attackRandomEnemy(enemiesInRange);
    }

    @Override
    public List<AiEffect> behave(MinerList<? extends GameObject> enemiesInRange) {
        return attackStrongestEnemies(enemiesInRange, FOCUSABLE_ON, AttackType.FIRE);
    }
}
