package com.to2gaming.thelastminer.aimodules.buildingsai.buildings;

import com.to2gaming.thelastminer.aimodules.buildingsai.Building;
import com.to2gaming.thelastminer.aimodules.interfaces.AiEffect;
import com.to2gaming.thelastminer.aimodules.interfaces.GameObject;
import com.to2gaming.thelastminer.manager.MinerList;
import com.to2gaming.thelastminer.phases.phaseBuilding.BuildingType;
import com.to2gaming.thelastminer.phases.phaseDefence.AttackType;

import java.util.List;


public class SorcererTower extends Building {
    private final int FOCUSABLE_ON = 4;
    private final int DMG_WHEN_DIES = 1000;
    private final int HEALTH = 60;
    private final int DMG = 150;
    private final int RANGE = 120;
    private final float DELAY = 2f;

    public SorcererTower(String imgPath) {
        super(imgPath);
        this.setName(BuildingType.SORCERER);
        this.setCurrentHealthPoints(HEALTH);
        this.setMaxHealthPoints(HEALTH);
        this.setDamagePoints(DMG);
        this.setDamageRange(RANGE);
        this.setDamageWhenDies(DMG_WHEN_DIES);
        this.setLevel(1);
        this.setDelay(DELAY);
    }

    @Override
    public List<AiEffect> die(MinerList<? extends GameObject> enemiesInRange) {
        return attackRandomEnemy(enemiesInRange);
    }

    @Override
    public List<AiEffect> behave(MinerList<? extends GameObject> enemiesInRange) {
        return attackStrongestEnemies(enemiesInRange, FOCUSABLE_ON, AttackType.ICE);
    }
}
