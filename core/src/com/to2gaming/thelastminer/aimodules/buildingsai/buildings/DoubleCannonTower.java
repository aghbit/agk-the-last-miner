package com.to2gaming.thelastminer.aimodules.buildingsai.buildings;

import com.to2gaming.thelastminer.aimodules.buildingsai.Building;
import com.to2gaming.thelastminer.aimodules.interfaces.AiEffect;
import com.to2gaming.thelastminer.aimodules.interfaces.GameObject;
import com.to2gaming.thelastminer.manager.MinerList;
import com.to2gaming.thelastminer.phases.phaseBuilding.BuildingType;

import java.util.ArrayList;
import java.util.List;

public class DoubleCannonTower extends Building {
    private final int DMG_WHEN_DIES = 0;
    private final int HEALTH = 150;
    private final int DMG = 125;
    private final int RANGE = 120;
    private final float DELAY = 1.25f;

    public DoubleCannonTower(String imgPath) {
        super(imgPath);
        this.setName(BuildingType.DOUBLE_CANNON);
        this.setDamageRange(this.RANGE);
        this.setMaxHealthPoints(this.HEALTH);
        this.setCurrentHealthPoints(this.HEALTH);
        this.setLevel(1);
        this.setDamagePoints(this.DMG);
        this.setDelay(DELAY);
    }

    @Override
    public List<AiEffect> die(MinerList<? extends GameObject> enemiesInRange) {
        return new ArrayList<>(); //still better than null
    }

    @Override
    public List<AiEffect> behave(MinerList<? extends GameObject> enemiesInRange) {
        return attackStrongestEnemy(enemiesInRange);
    }
}
