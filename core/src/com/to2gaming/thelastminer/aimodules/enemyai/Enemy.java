package com.to2gaming.thelastminer.aimodules.enemyai;

import com.badlogic.gdx.Gdx;
import com.to2gaming.thelastminer.aimodules.enemyai.behaviours.attack.AttackBehaviour;
import com.to2gaming.thelastminer.aimodules.enemyai.behaviours.attack.BasicAttackBehaviour;
import com.to2gaming.thelastminer.aimodules.enemyai.behaviours.die.BasicDieBehaviour;
import com.to2gaming.thelastminer.aimodules.enemyai.behaviours.die.DieBehaviour;
import com.to2gaming.thelastminer.aimodules.enemyai.behaviours.move.GroundMoveBehaviour;
import com.to2gaming.thelastminer.aimodules.enemyai.behaviours.move.MoveBehaviour;
import com.to2gaming.thelastminer.aimodules.interfaces.AiEffect;
import com.to2gaming.thelastminer.aimodules.interfaces.GameObject;
import com.to2gaming.thelastminer.manager.MinerList;

import java.util.List;


public abstract class Enemy extends GameObject {
    private int dmg;
    private float speed;
    private int hp;
    private int range;

    protected String name;

    protected boolean isAlive;

    protected MoveBehaviour moveBehaviour = new GroundMoveBehaviour();

    protected AttackBehaviour attackBehaviour = new BasicAttackBehaviour();

    protected DieBehaviour dieBehaviour = new BasicDieBehaviour();

    public Enemy(float x, float y) {
        setAnimationAsActor("enemies/zombie.atlas", 0.2f);
        getActor().setWidth(36);
        getActor().setHeight(36);
        this.setX(x);
        this.setY(y);
        this.isAlive = true;
    }

    @Override
    public List<AiEffect> behave(MinerList<? extends GameObject> buildings) {
        float time = Gdx.graphics.getDeltaTime();
        getActor().act(time);
        move(time, buildings);
        return attack(time, buildings);

    }
    
    @Override
    public boolean isAlive() {
        return this.isAlive;
    }

    @Override
    public boolean getHit(Integer dmg) {
        if (this.isAlive) {
            this.hp -= dmg;
            if (this.hp < 1) {
                this.isAlive = false;
                return false;
            }
        }
        return true;
    }

    public void move(float time, MinerList<? extends GameObject> buildings) {
        this.moveBehaviour.move(this, time, buildings);
    }

    public List<AiEffect> attack(float time, MinerList<? extends GameObject> buildings) {
        return this.attackBehaviour.attack(this, time, buildings);
    }

    public List<AiEffect> die(MinerList<? extends GameObject> objects) {
        return this.dieBehaviour.die(objects, this);
    }

    @Override
    public int getDamageRange() {
        return range;
    }

    protected void setRange(int range){
        this.range = range;
    }

    public float getSpeed() {
        return speed;
    }

    protected void setSpeed(float speed){
        this.speed = speed;
    }

    public int getHp() {
        return hp;
    }

    public void setHp(int hp) {
        this.hp = hp;
    }

    @Override
    public float getX() {
        return super.getX();
    }

    public void setX(float x) {
        super.getActor().setX(x);
    }

    @Override
    public float getY() {
        return super.getY();
    }

    public void setY(float y) {
        super.getActor().setY(y);
    }

    public int getDmg() {
        return dmg;
    }

    protected void setDmg(int dmg){
        this.dmg = dmg;
    }

    public float getHeight() {
        return super.getActor().getHeight();
    }
    public float getWidth() {
        return super.getActor().getWidth();
    }
    public String getName() {
        return name;
    }

    public void setIsAlive(boolean isAlive) {
        this.isAlive = isAlive;
    } //todo

    public MoveBehaviour getMoveBehaviour() {
        return moveBehaviour;
    }

    public void setMoveBehaviour(MoveBehaviour moveBehaviour) {
        this.moveBehaviour = moveBehaviour;
    }

    public AttackBehaviour getAttackBehaviour() {
        return attackBehaviour;
    }

    public void setAttackBehaviour(AttackBehaviour attackBehaviour) {
        this.attackBehaviour = attackBehaviour;
    }

    public DieBehaviour getDieBehaviour() {
        return dieBehaviour;
    }

    public void setDieBehaviour(DieBehaviour dieBehaviour) {
        this.dieBehaviour = dieBehaviour;
    }
}
