package com.to2gaming.thelastminer.aimodules.enemyai.behaviours.move;

import com.to2gaming.thelastminer.aimodules.enemyai.Enemy;
import com.to2gaming.thelastminer.aimodules.interfaces.GameObject;
import com.to2gaming.thelastminer.manager.MinerList;

public interface MoveBehaviour {
    public void move(Enemy en, float time, MinerList<? extends GameObject> objects);


}