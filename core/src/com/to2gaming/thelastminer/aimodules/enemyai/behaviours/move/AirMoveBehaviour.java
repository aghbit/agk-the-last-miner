package com.to2gaming.thelastminer.aimodules.enemyai.behaviours.move;

import com.badlogic.gdx.math.Vector2;
import com.to2gaming.thelastminer.aimodules.enemyai.Enemy;
import com.to2gaming.thelastminer.aimodules.interfaces.GameObject;
import com.to2gaming.thelastminer.manager.MinerList;
import com.to2gaming.thelastminer.ui.util.UIConstants;

public class AirMoveBehaviour implements MoveBehaviour {

    private float halfHeight = UIConstants.SCREEN_HEIGHT / 2;
    private float halfWidth = UIConstants.SCREEN_WIDTH / 2;

    public void move(Enemy en, float time, MinerList<? extends GameObject> objects) {
        float dist = time * en.getSpeed();
        Vector2 v = new Vector2();
        v.x = halfWidth - en.getX();
        v.y = halfHeight - en.getY();
        v.nor();
        v.setLength(dist);

        en.setPosition(en.getX() + v.x, en.getY() + v.y);
    }
}
