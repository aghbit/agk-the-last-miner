package com.to2gaming.thelastminer.aimodules.enemyai.behaviours.attack;

import com.to2gaming.thelastminer.aimodules.enemyai.Enemy;
import com.to2gaming.thelastminer.aimodules.interfaces.AiEffect;
import com.to2gaming.thelastminer.aimodules.interfaces.GameObject;
import com.to2gaming.thelastminer.manager.MinerList;

import java.util.List;

public interface AttackBehaviour {
    List<AiEffect> attack(Enemy en, float time, MinerList<? extends GameObject> objects);
}
