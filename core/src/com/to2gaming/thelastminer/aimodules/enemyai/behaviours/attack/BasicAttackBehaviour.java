package com.to2gaming.thelastminer.aimodules.enemyai.behaviours.attack;

import com.to2gaming.thelastminer.aimodules.enemyai.Enemy;
import com.to2gaming.thelastminer.aimodules.interfaces.AiEffect;
import com.to2gaming.thelastminer.aimodules.interfaces.GameObject;
import com.to2gaming.thelastminer.manager.MinerList;
import com.to2gaming.thelastminer.phases.phaseDefence.Attack;
import com.to2gaming.thelastminer.phases.phaseDefence.AttackType;

import java.util.ArrayList;
import java.util.List;

public class BasicAttackBehaviour implements AttackBehaviour {

    public List<AiEffect> attack(Enemy en, float time, MinerList<? extends GameObject> objects) {
        List<AiEffect> l = new ArrayList<AiEffect>();
        for (GameObject o: objects) {
            if (((en.getX()-o.getX())*(en.getX()-o.getX())) + ((en.getX()-o.getX())*(en.getX()-o.getX()))
                    < en.getDamageRange()) {
                AttackType attackType = null;
                AiEffect attack = new Attack(o, attackType, en.getDmg());
                l.add(attack);
                break;
            }
        }
        return l;
    }
}