package com.to2gaming.thelastminer.aimodules.enemyai.behaviours.move;

import com.badlogic.gdx.math.Vector2;
import com.to2gaming.thelastminer.aimodules.enemyai.Enemy;
import com.to2gaming.thelastminer.aimodules.interfaces.GameObject;
import com.to2gaming.thelastminer.manager.MinerList;
import com.to2gaming.thelastminer.ui.util.UIConstants;

public class GroundMoveBehaviour implements MoveBehaviour {

    private float halfHeight = UIConstants.SCREEN_HEIGHT / 2;
    private float halfWidth = UIConstants.SCREEN_WIDTH / 2;

    Vector2 v = null;
    Vector2 v2 = null;





    public void subMove(Enemy en, float dist, float x, float y) {

        v.x = x - en.getX();
        v.y = y - en.getY();
        v.nor();
        v.setLength(dist);
        v2.x = en.getX() - v.x;
        v2.y = en.getY() - v.y;
        en.setPosition(v2.x, v2.y);
    }

    public boolean checkMove(float x, float y, Enemy en, float dist, MinerList<? extends GameObject> buildings) {
        boolean canMove = true;
        v.x = x - en.getX();
        v.y = y - en.getY();
        v.nor();
        v.setLength(dist);

        for (GameObject b : buildings) {
            if (v.x > b.getX() && v.x < (b.getX() + b.getActor().getWidth())
                    && v.y > b.getY() && v.y < (b.getY() + b.getActor().getHeight())) {
                canMove = false;
                break;
            }
        }
        return canMove;
    }

    public void move(Enemy en, float time, MinerList<? extends GameObject> buildings) {
        float dist = time * en.getSpeed();

        if (checkMove(halfWidth, halfHeight, en, dist, buildings)) {
            subMove(en, dist, halfWidth, halfHeight);

        } else if (checkMove(en.getX(), 0, en, dist, buildings)) {
            subMove(en, dist, en.getX(), 0);
        }

        else if (checkMove(2 * halfWidth, en.getY(), en, dist, buildings)) {
            subMove(en, dist, 2 * halfWidth, en.getY());
        }

        else if (checkMove(en.getX(),2 * halfHeight,en,dist,buildings)) {
            subMove(en, dist, en.getX(),2 * halfHeight);
        }
        else if (checkMove(0,en.getY(),en,dist,buildings)) {
            subMove(en, dist,0, en.getY());
        }
    }
}
