package com.to2gaming.thelastminer.aimodules.enemyai.enemies;


import com.to2gaming.thelastminer.aimodules.enemyai.behaviours.attack.BasicAttackBehaviour;
import com.to2gaming.thelastminer.aimodules.enemyai.behaviours.die.BasicDieBehaviour;
import com.to2gaming.thelastminer.aimodules.enemyai.behaviours.move.AirMoveBehaviour;
import com.to2gaming.thelastminer.aimodules.enemyai.Enemy;

public class BasicEnemy3 extends Enemy {

    //    private int dmg = 5;
//    private float speed = 5;
//    private int hp = 20;
    private float width = 5;
    private float height = 5;
//    private int range = 10;

    private String name = "Basic3";

    public BasicEnemy3(float x, float y) {
        super(x, y);
        setAnimationAsActor("enemies/wonzrzeczny.atlas", 0.2f);
        getActor().setWidth(48);
        getActor().setHeight(48);
        setDmg(5);
        setSpeed(50);
        setHp(15);
        setRange(20);
        this.moveBehaviour = new AirMoveBehaviour();
        this.attackBehaviour = new BasicAttackBehaviour();
        this.dieBehaviour = new BasicDieBehaviour();
    }
}
