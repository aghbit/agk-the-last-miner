package com.to2gaming.thelastminer.aimodules.enemyai.enemies;

import com.to2gaming.thelastminer.aimodules.enemyai.Enemy;
import com.to2gaming.thelastminer.aimodules.enemyai.behaviours.attack.NoAttackBehaviour;
import com.to2gaming.thelastminer.aimodules.enemyai.behaviours.die.BasicDieBehaviour;
import com.to2gaming.thelastminer.aimodules.enemyai.behaviours.move.GroundMoveBehaviour;

public class Harpy extends Enemy{
    private int dmg = 0;
    private float speed = 10;
    private int hp = 50;
    private float width = 4;
    private float height = 4;
    private int range = 0;

    private String name = "Mole";

    public Harpy(float x, float y) {
        super(x, y);
        this.moveBehaviour = new GroundMoveBehaviour();
        this.attackBehaviour = new NoAttackBehaviour();
        this.dieBehaviour = new BasicDieBehaviour();
    }
}
