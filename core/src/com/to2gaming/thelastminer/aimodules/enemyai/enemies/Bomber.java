package com.to2gaming.thelastminer.aimodules.enemyai.enemies;


import com.to2gaming.thelastminer.aimodules.enemyai.Enemy;
import com.to2gaming.thelastminer.aimodules.enemyai.behaviours.attack.BasicAttackBehaviour;
import com.to2gaming.thelastminer.aimodules.enemyai.behaviours.die.BasicDieBehaviour;
import com.to2gaming.thelastminer.aimodules.enemyai.behaviours.move.GroundMoveBehaviour;

public class Bomber extends Enemy{
    private int dmg = 20;
    private float speed = 10;
    private int hp = 40;
    private float width = 4;
    private float height = 4;
    private int range = 20;

    private String name = "Bomber";

    public Bomber(float x, float y) {
        super(x, y);
        this.moveBehaviour = new GroundMoveBehaviour();
        this.attackBehaviour = new BasicAttackBehaviour();
        this.dieBehaviour = new BasicDieBehaviour();
    }
}
