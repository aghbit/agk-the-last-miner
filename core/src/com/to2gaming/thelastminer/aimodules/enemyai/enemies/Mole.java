package com.to2gaming.thelastminer.aimodules.enemyai.enemies;

import com.to2gaming.thelastminer.aimodules.enemyai.Enemy;
import com.to2gaming.thelastminer.aimodules.enemyai.behaviours.attack.NoAttackBehaviour;
import com.to2gaming.thelastminer.aimodules.enemyai.behaviours.die.BasicDieBehaviour;
import com.to2gaming.thelastminer.aimodules.enemyai.behaviours.move.GroundMoveBehaviour;

public class Mole extends Enemy{
    private int dmg = 0;
    private float speed = 2;
    private int hp = 200;
    private float width = 4;
    private float height = 8;
    private int range = 0;

    private String name = "Mole";

    public Mole(float x, float y) {
        super(x, y);
        this.moveBehaviour = new GroundMoveBehaviour();
        this.attackBehaviour = new NoAttackBehaviour();
        this.dieBehaviour = new BasicDieBehaviour();
    }
}
