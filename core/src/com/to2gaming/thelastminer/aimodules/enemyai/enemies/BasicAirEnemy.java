package com.to2gaming.thelastminer.aimodules.enemyai.enemies;

import com.to2gaming.thelastminer.aimodules.enemyai.Enemy;
import com.to2gaming.thelastminer.aimodules.enemyai.behaviours.attack.BasicAttackBehaviour;
import com.to2gaming.thelastminer.aimodules.enemyai.behaviours.die.BasicDieBehaviour;
import com.to2gaming.thelastminer.aimodules.enemyai.behaviours.move.AirMoveBehaviour;

public class BasicAirEnemy extends Enemy {
    private int dmg = 0;
    private float speed = 10;
    private int hp = 20;
    private float width = 5;
    private float height = 5;
    private int range = 0;

    private String name = "BasicAir";

    public BasicAirEnemy(float x, float y) {
        super(x, y);
        this.moveBehaviour = new AirMoveBehaviour();
        this.attackBehaviour = new BasicAttackBehaviour();
        this.dieBehaviour = new BasicDieBehaviour();
    }
}
