package com.to2gaming.thelastminer.aimodules.enemyai.enemies;


import com.to2gaming.thelastminer.aimodules.enemyai.behaviours.attack.BasicAttackBehaviour;
import com.to2gaming.thelastminer.aimodules.enemyai.behaviours.die.BasicDieBehaviour;
import com.to2gaming.thelastminer.aimodules.enemyai.behaviours.move.AirMoveBehaviour;
import com.to2gaming.thelastminer.aimodules.enemyai.Enemy;

public class BasicEnemy2 extends Enemy {

    //    private int dmg = 5;
//    private float speed = 5;
//    private int hp = 20;
    private float width = 5;
    private float height = 5;
//    private int range = 10;

    private String name = "Basic2";

    public BasicEnemy2(float x, float y) {
        super(x, y);
        setAnimationAsActor("enemies/jadaczscierwa.atlas", 0.2f);
        getActor().setWidth(36);
        getActor().setHeight(36);
        setDmg(2);
        setSpeed(80);
        setHp(5);
        setRange(20);
        this.moveBehaviour = new AirMoveBehaviour();
        this.attackBehaviour = new BasicAttackBehaviour();
        this.dieBehaviour = new BasicDieBehaviour();
    }
}
