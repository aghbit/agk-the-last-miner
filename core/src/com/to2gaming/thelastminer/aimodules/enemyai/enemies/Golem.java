package com.to2gaming.thelastminer.aimodules.enemyai.enemies;

import com.to2gaming.thelastminer.aimodules.enemyai.Enemy;
import com.to2gaming.thelastminer.aimodules.enemyai.behaviours.attack.NoAttackBehaviour;
import com.to2gaming.thelastminer.aimodules.enemyai.behaviours.die.BasicDieBehaviour;
import com.to2gaming.thelastminer.aimodules.enemyai.behaviours.move.GroundMoveBehaviour;

public class Golem extends Enemy{
    private int dmg = 40;
    private float speed = 3;
    private int hp = 500;
    private float width = 10;
    private float height = 10;
    private int range = 10;
    private boolean resurrect = true;

    private String name = "Golem";

    public Golem(float x, float y) {
        super(x, y);
        this.moveBehaviour = new GroundMoveBehaviour();
        this.attackBehaviour = new NoAttackBehaviour();
        this.dieBehaviour = new BasicDieBehaviour();
    }

    @Override
    public boolean getHit(Integer dmg) {
        if (this.isAlive) {
            this.hp -= dmg;
            if (this.hp < 1) {
                if (this.resurrect == true) {
                    resurrect = false;
                }
                else {
                    this.isAlive = false;
                    return false;
                }
            }
        }
        return true;
    }
}
