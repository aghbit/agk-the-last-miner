package com.to2gaming.thelastminer.aimodules.enemyai.enemies;

import com.to2gaming.thelastminer.aimodules.enemyai.Enemy;
import com.to2gaming.thelastminer.aimodules.enemyai.behaviours.attack.NoAttackBehaviour;
import com.to2gaming.thelastminer.aimodules.enemyai.behaviours.die.BasicDieBehaviour;
import com.to2gaming.thelastminer.aimodules.enemyai.behaviours.move.GroundMoveBehaviour;
import com.to2gaming.thelastminer.aimodules.interfaces.AiEffect;
import com.to2gaming.thelastminer.aimodules.interfaces.GameObject;
import com.to2gaming.thelastminer.manager.MinerList;

import java.util.List;

public class GrizzlyBear extends Enemy {
    private int dmg = 0;
    private float speed = 5;
    private int hp = 400;
    private float width = 8;
    private float height = 8;
    private int range = 0;

    private String name = "GrizzlyBear";

    public GrizzlyBear(float x, float y) {
        super(x, y);
        this.moveBehaviour = new GroundMoveBehaviour();
        this.attackBehaviour = new NoAttackBehaviour();
        this.dieBehaviour = new BasicDieBehaviour();
    }

    @Override
    public List<AiEffect> behave(MinerList<? extends GameObject> objects) {
        int time = 10;
        if (this.hp < 400) {
            this.hp += 10;
        }
        this.move(time, objects);
        return this.attack(time, objects);
    }
}
