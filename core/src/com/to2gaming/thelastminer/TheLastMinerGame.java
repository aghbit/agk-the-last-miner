package com.to2gaming.thelastminer;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.to2gaming.thelastminer.manager.Difficulty;
import com.to2gaming.thelastminer.manager.GameData;
import com.to2gaming.thelastminer.manager.GamePhase;
import com.to2gaming.thelastminer.phases.phaseBuilding.BuildingPhase;
import com.to2gaming.thelastminer.phases.phaseDefence.DefencePhase;
import com.to2gaming.thelastminer.ui.effects.Effect;
import com.to2gaming.thelastminer.ui.UIModule;

public class TheLastMinerGame extends Game {

	GamePhase gamePhase;
	GameData gameData;
	UIModule uiModule;
	BuildingPhase buildingPhaseModule;
	DefencePhase defencePhaseModule;

    float timer;

	//flagi
	boolean startGameFlag = false;
	boolean startFightingSceneFlag = false;
	boolean startBuildingSceneFlag = false;
	
	@Override
	public void create () {
        gamePhase = GamePhase.START;
		gameData = new GameData();

		buildingPhaseModule = new BuildingPhase(this);
		defencePhaseModule = new DefencePhase(this);
		uiModule = new UIModule(this, buildingPhaseModule, defencePhaseModule);
	}

	public void update(float deltaTime) {
		switch (gamePhase) {
			case START:
				updateStartPhase();
				break;
			case BUILDING:
				updateBuildingPhase(deltaTime);
				break;
			case FIGHTING:
				updateFightingPhase(deltaTime);
				break;
		}
	}


	@Override
	public void render () {
		float delta = Gdx.graphics.getDeltaTime();
		this.update(delta);
		uiModule.render(gameData, gamePhase);
	}

	public void startGame(String nick, Difficulty difficulty){
		if (gamePhase == GamePhase.START) {
			setGameData(nick, difficulty, 1, 5000);
			startGameFlag = true;
		}
	}

    public void endBuildingPhase() {
        if (gamePhase == GamePhase.BUILDING) {
            startFightingSceneFlag = true;
        }
    }

	public void endFightingPhase() {
		if (gamePhase == GamePhase.FIGHTING){
			startBuildingSceneFlag = true;
		}
	}

	private void updateStartPhase() {
		if(startGameFlag) {
			startGameFlag = false;
			prepareBuildingPhase();
		}
	}

	private void updateBuildingPhase(float deltaTime) {
		gameData.updateEffects(deltaTime);
		buildingPhaseModule.update();
		timer -= deltaTime;
		if(timer < 0 || startFightingSceneFlag) {
			startFightingSceneFlag = false;
			buildingPhaseModule.deactivate();
			gameData.setCash(buildingPhaseModule.getCash());
			gamePhase = GamePhase.FIGHTING;
			gameData.generateEnemies();
			defencePhaseModule.setNewData(gameData.getBuildingList(), gameData.getEnemyList(), gameData.getSkills(), gameData.getLevel(), gameData.getMana());
			defencePhaseModule.activate();
		}
	}

	private void updateFightingPhase(float deltaTime) {
		gameData.updateEffects(deltaTime);
		defencePhaseModule.update();
		if(startBuildingSceneFlag) {
			startBuildingSceneFlag = false;
			if (defencePhaseModule.getResult()){
				defencePhaseModule.deactivate();
				gameData.setLevel(gameData.getLevel() + 1);
				prepareBuildingPhase();
			} else {
				uiModule.endGame();
				gamePhase = GamePhase.GAME_OVER;
			}
		}
	}

	private void setGameData(String nick, Difficulty difficulty, int level, int cash){
		gameData.setNick(nick);
		gameData.setDifficulty(difficulty);
		gameData.setLevel(level);
		gameData.setCash(cash);
	}

	private void prepareBuildingPhase() {
		gamePhase = GamePhase.BUILDING;
		// TODO: tell DefencePhase to delete buildings also from grid
		buildingPhaseModule.setNewData(gameData.getBuildingList(), gameData.getCash());
		buildingPhaseModule.activate();
		timer = 10f;
	}

    public void addEffect(Effect e) {
        gameData.getEffectList().add(e);
    }

    public void removeEffect(Effect e) {
        gameData.getEffectList().remove(e);
    }
}
