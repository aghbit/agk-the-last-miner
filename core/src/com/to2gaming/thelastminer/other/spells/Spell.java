package com.to2gaming.thelastminer.other.spells;

import com.badlogic.gdx.scenes.scene2d.Actor;
import com.to2gaming.thelastminer.phases.phaseDefence.AttackType;

/**
 * @author Dariusz Ciechanowski
 * @author Mateusz Gawel
 */

public class Spell extends Actor implements ISpell {

    private final AttackType attackType;
    private final int requiredMana;
    private final int damage;
    private final float range;
    private final Integer spellLevel;

    private final float posX;
    private final float posY;

    private Spell(SpellBuilder builder) {
        this.attackType = builder.attackType;
        this.requiredMana = builder.requiredMana;
        this.damage = builder.damage;
        this.range = builder.range;
        this.spellLevel = builder.spellLevel;
        this.posX = builder.posX;
        this.posY = builder.posY;
    }

    public AttackType getAttackType() {
        return attackType;
    }

    public float getDamageRange() {
        return range;
    }

    public int getRequiredMana() {
        return requiredMana;
    }

    public Integer getDamageValue() {
        return new Integer(damage);
    }

    public Integer getSpellLevel() {
        return spellLevel;
    }

    @Override
    public float getX() {
        return posX;
    }

    @Override
    public float getY() {
        return posY;
    }

    @Override
    public int getDmg() {
        return getDamageValue();
    }

    public static class SpellBuilder {
        private AttackType attackType;
        private int requiredMana;
        private int damage;
        private float range;
        private Integer spellLevel;

        private float posX;
        private float posY;

        public SpellBuilder attackType(AttackType attackType) {
            this.attackType = attackType;
            return this;
        }

        public SpellBuilder requiredMana(int requiredMana) {
            this.requiredMana = requiredMana;
            return this;
        }

        public SpellBuilder damage(int damage) {
            this.damage = damage;
            return this;
        }

        public SpellBuilder range(float range) {
            this.range = range;
            return this;
        }

        public SpellBuilder spellLevel(Integer spellLevel) {
            this.spellLevel = spellLevel;
            return this;
        }

        public SpellBuilder posX(float posX) {
            this.posX = posX;
            return this;
        }

        public SpellBuilder posY(float posY) {
            this.posY = posY;
            return this;
        }

        public Spell build() {
            return new Spell(this);
        }
    }

}