package com.to2gaming.thelastminer.other.spells;

import com.to2gaming.thelastminer.ui.ObjectWithActor;

/**
 * @author Dariusz Ciechanowski
 * @author Mateusz Gawel
 */

public class SpellType extends ObjectWithActor {
    private String name;
    private Integer spellLevel;

    public SpellType(String path, String name) {
        setImageAsActor(path);
        setName(name);
    }

    public Integer getSpellLevel() {
        return spellLevel;
    }

    public String getName() {
        return name;
    }

    public void setSpellLevel(Integer spellLevel) {
        this.spellLevel = spellLevel;
    }

    public void setName(String name) {
        this.name = name;
    }
}


