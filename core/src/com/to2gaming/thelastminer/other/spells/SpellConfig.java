package com.to2gaming.thelastminer.other.spells;


import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.scenes.scene2d.ui.Image;

public class SpellConfig extends SpellType { //FIXME taki szybki myk, nie wiem czy moze tak byc
    private final String name;
    private final String iconPath;
    private final Image icon;

    private SpellConfig(SpellConfigBuilder builder) {
        super(builder.iconPath, builder.name);
        this.name = builder.name;
        this.iconPath = builder.iconPath;
        this.icon = new Image(new Texture(iconPath));
    }

    public String getName() {
        return name;
    }

    public String getIconPath() {
        return iconPath;
    }

    public Image getIcon() {
        return icon;
    }

    public static class SpellConfigBuilder {
        private String name;
        private String iconPath;

        public SpellConfigBuilder name(String name) {
            this.name = name;
            return this;
        }

        public SpellConfigBuilder iconPath(String iconPath) {
            this.iconPath = iconPath;
            return this;
        }

        public SpellConfig build() {
            return new SpellConfig(this);
        }
    }

}
