package com.to2gaming.thelastminer.other.spells;

import com.to2gaming.thelastminer.other.spells.Spell.SpellBuilder;

import java.util.List;
import java.util.Optional;

/**
 * @author Dariusz Ciechanowski
 * @author Mateusz Gawel
 */

public interface SpellRepository {

    List<String> getSpellNames();

    Optional<SpellBuilder> getSpell(String name);

    Optional<SpellConfig.SpellConfigBuilder> getSpellConfig(String name);
}
