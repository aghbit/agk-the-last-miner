package com.to2gaming.thelastminer.other.spells;

import com.to2gaming.thelastminer.other.spells.Spell.SpellBuilder;
import com.to2gaming.thelastminer.phases.phaseDefence.AttackType;

import java.util.*;

/**
 * @author Dariusz Ciechanowski
 * @author Mateusz Gaweł
 */

public class HardcodedSpellDb implements SpellRepository {

    private final Map<String, SpellBuilder> spells;
    private final Map<String, SpellConfig.SpellConfigBuilder> spellConfigs;
    private final static StringBuilder spellImagePath = new StringBuilder("spells/");

    public HardcodedSpellDb() {
        spells = new HashMap<>();
        spellConfigs = new HashMap<>();

        SpellBuilder basicAttack = new SpellBuilder()
                .attackType(AttackType.IRON)
                .damage(10)
                .range(30)
                .requiredMana(0);

        SpellBuilder fireBall = new SpellBuilder()
                .attackType(AttackType.FIRE)
                .damage(100)
                .range(100)
                .requiredMana(10);

        SpellBuilder blizzard = new SpellBuilder()
                .attackType(AttackType.ICE)
                .damage(50)
                .range(200)
                .requiredMana(50);

        spells.put("basic", basicAttack);
        spells.put("fireBall", fireBall);
        spells.put("blizzard", blizzard);

        for (String key : spells.keySet()) {
            SpellConfig.SpellConfigBuilder spellConfig = new SpellConfig.SpellConfigBuilder()
                    .name(key)
                    .iconPath(getIconPath(key));
            spellConfigs.put(key, spellConfig);
        }
    }

    @Override
    public List<String> getSpellNames() {
        List<String> spellNames = new LinkedList<>();

        spells.keySet().forEach(name -> spellNames.add(name));

        return spellNames;
    }

    @Override
    public Optional<SpellBuilder> getSpell(String name) {
        if (spells.containsKey(name)) {
            return Optional.of(spells.get(name));
        }
        return Optional.empty();
    }

    @Override
    public Optional<SpellConfig.SpellConfigBuilder> getSpellConfig(String name) {
        if (spellConfigs.containsKey(name)) {
            return Optional.of(spellConfigs.get(name));
        }
        return Optional.empty();
    }

    private String getIconPath(String name) {
        return spellImagePath.append(name).append(".png").toString();
    }

}