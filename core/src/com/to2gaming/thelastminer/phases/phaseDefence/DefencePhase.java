package com.to2gaming.thelastminer.phases.phaseDefence;

import com.badlogic.gdx.Gdx;
import com.to2gaming.thelastminer.TheLastMinerGame;
import com.to2gaming.thelastminer.aimodules.enemyai.Enemy;
import com.to2gaming.thelastminer.aimodules.buildingsai.Building;
import com.to2gaming.thelastminer.aimodules.interfaces.AiEffect;
import com.to2gaming.thelastminer.aimodules.interfaces.GameObject;
import com.to2gaming.thelastminer.manager.MinerList;
import com.to2gaming.thelastminer.other.spells.*;
import com.to2gaming.thelastminer.phases.interfaces.IPhase;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * @author Dariusz Ciechanowski
 * @author Mateusz Gawel
 */

public class DefencePhase implements IPhase {

	private final TheLastMinerGame manager;
	private final SpellRepository spellReposotory;
	private final List<Spell> spellsToCast;

	private MinerList<Building> buildings;
	private MinerList<Enemy> enemies;
	private int playerMana;

	private long phaseStartTime;
	private int level;
	private boolean active;
	private boolean enemyInTheMiddle;


	public DefencePhase(TheLastMinerGame manager) {
		this.manager = manager;
		this.spellReposotory = new HardcodedSpellDb();
		this.spellsToCast = new LinkedList<>();
	}

	public void setNewData(MinerList<Building> buildings, MinerList<Enemy> enemies, Set<SpellType> skillSet, int level, int mana) {
		this.buildings = buildings;
		this.enemies = enemies;
		this.playerMana = mana;
		this.level = level;
		this.phaseStartTime = System.currentTimeMillis();
		this.enemyInTheMiddle = false;
	}

	@Override
	public void update() {
		List<AiEffect> entitiesBehaviour = new LinkedList<>();

		entitiesBehaviour.addAll(enemyBehaviour());
		entitiesBehaviour.addAll(buildingBehaviour());
		spellExecute();

		entitiesBehaviour.forEach(action -> action.doIt());

		if (phaseShouldEnd()) {
			manager.endFightingPhase();
		}
	}

	private Integer getSpellLevel(String spellName) {
		return new Integer(1);
	}

	public void useSpell(SpellType spellType, float x, float y) {
		String spellName = spellType.getName();

		Integer spellLevel = getSpellLevel(spellName);
		spellType.setSpellLevel(spellLevel);

		Optional<Spell.SpellBuilder> spell = spellReposotory.getSpell(spellName);

		if (spell.isPresent()) {
			Spell castableSpell = spell.get()
					.posX(x)
					.posY(y)
					.spellLevel(spellLevel)
					.build();

			if (playerMana >= castableSpell.getRequiredMana()) {
				decreasePlayerMana(castableSpell.getRequiredMana());
				spellsToCast.add(castableSpell);
			}
		}
	}

	public List<SpellConfig> getSpellConfigs() {
		List<SpellConfig> spellConfigs = new LinkedList<>();
		List<String> spellNames = spellReposotory.getSpellNames();

		for (String name : spellNames) {
			Optional<SpellConfig.SpellConfigBuilder> builder = spellReposotory.getSpellConfig(name);
			if (builder.isPresent()) {
				spellConfigs.add(builder.get().build());
			}
		}

		return spellConfigs;
	}

	private List<AiEffect> enemyBehaviour() {
		MinerList<Building> buildingsInRange = new MinerList<Building>();
		List<AiEffect> enemyActions = new LinkedList<>();

		MinerList<Enemy> enemies2 = new MinerList<>();
		enemies2.addAll(enemies);

		for (Enemy enemy : enemies2) {
			buildingsInRange = buildings.getElementsInRange(enemy.getX(), enemy.getY(), enemy.getDamageRange());

			if (enemy.isAlive()) {
				enemyActions.addAll(enemy.behave(buildingsInRange));
			} else {
				enemyActions.addAll(enemy.die(buildingsInRange));
				enemies.remove(enemy);
                enemy.destroyActor();
			}
		}
		return enemyActions;
	}

	private List<AiEffect> buildingBehaviour() {
		MinerList<Enemy> enemyInRange;
		List<AiEffect> buildingActions = new LinkedList<>();

		MinerList<Building> buildings2 = new MinerList<>();
		buildings2.addAll(buildings);
		for (Building building : buildings2) {
            building.update(Gdx.graphics.getDeltaTime());
			enemyInRange = enemies.getElementsInRange(building.getX(), building.getY(), building.getDamageRange());
			
			if (building.isAlive()) {
				if(building.isReady())
					buildingActions.addAll(building.behave(enemyInRange));
			} else {
				buildingActions.addAll(building.die(enemyInRange));
				buildings.remove(building);
                building.destroyActor();
			}

		}

		return buildingActions;
	}

	private void spellExecute() {
		AiEffect spellAction;

		for (Spell spell : spellsToCast) {
			List<AiEffect> spellActions = new LinkedList<>();
			MinerList<Enemy> enemyInRange = enemies.getElementsInRange(spell.getX(), spell.getY(), spell.getDamageRange());

			for (Enemy enemy : enemyInRange) {
				spellAction = new Attack(enemy, spell.getAttackType(), spell.getDamageValue());
				spellActions.add(spellAction);
			}
			spellActions.forEach(action -> action.doIt());
		}

		spellsToCast.clear();
	}


	private boolean phaseShouldEnd() {
//		if(System.currentTimeMillis() - phaseStartTime > 15000) { // XXX: only for debug
//			deactivate();
//		}
		if(enemies.isEmpty()){
			deactivate();
		}
		if(enemies.anyoneInTheMiddle()) {
			enemyInTheMiddle = true;
			deactivate();
		}

		return !active;
	}


	public boolean getResult() {
		return !enemyInTheMiddle;
	}

	@Override
	public void activate() {
		active = true;
	}

	@Override
	public void deactivate() {
		active = false;
	}

	public double getMana() {
		return playerMana;
	}

	private void increasePlayerMana(double amount) {
		playerMana += amount;
	}

	private void decreasePlayerMana(double amount) {
		playerMana -= amount;
	}

}
