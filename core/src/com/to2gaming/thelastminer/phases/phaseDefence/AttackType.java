package com.to2gaming.thelastminer.phases.phaseDefence;

public enum AttackType {
    FIRE,
    ICE,
    IRON
}