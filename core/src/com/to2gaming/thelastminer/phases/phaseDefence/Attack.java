package com.to2gaming.thelastminer.phases.phaseDefence;

import com.to2gaming.thelastminer.aimodules.interfaces.AiEffect;
import com.to2gaming.thelastminer.aimodules.interfaces.ObjectAI;

/**
 * @author Dariusz Ciechanowski
 * @author Mateusz Gaweł
 */

public class Attack implements AiEffect {

    private ObjectAI target;
    private AttackType attackType;
    private Integer value;

    public Attack(ObjectAI target, AttackType attackType, Integer value) {
        this.target = target;
        this.attackType = attackType;
        this.value = value;
    }

    @Override
    public void doIt() {
        Integer damage = DamageCalculator.getFinalDamage(target, attackType, value);
        target.getHit(damage);
    }

    @Override
    public ObjectAI getTarget() {
        return target;
    }

}
