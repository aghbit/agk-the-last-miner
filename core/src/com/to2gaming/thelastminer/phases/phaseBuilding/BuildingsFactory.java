package com.to2gaming.thelastminer.phases.phaseBuilding;

import com.to2gaming.thelastminer.aimodules.buildingsai.Building;
import com.to2gaming.thelastminer.aimodules.buildingsai.buildings.*;
import com.to2gaming.thelastminer.ui.util.UILogger;

public class BuildingsFactory {
    public static Building getBuilding(BuildingType type) {
        switch (type) {
            case CANNON:
                return new CannonTower(getPath(type));
            case MACHINE_GUN:
                return new MachineGunTower(getPath(type));
            case TWO_MACHINE_GUN:
                return new TwoMachineGunsTower(getPath(type));
            case WIZARD:
                return new WizardTower(getPath(type));
            case SORCERER:
                return new SorcererTower(getPath(type));
            case DOUBLE_CANNON:
                return new DoubleCannonTower(getPath(type));
            case MINE:
                return new WizardTower(getPath(type));
        }
        return null; //Sorry, not much time ;(
    }

    public static String getPath(BuildingType type) {
        switch(type) {
            case CANNON: return "buildings/CannonTower.png";
            case MACHINE_GUN: return "buildings/MachineGunTower.png";
            case TWO_MACHINE_GUN: return "buildings/TwoMachineGunTower.png";
            case WIZARD: return "buildings/WizardTower.png";
            case MINE: return "buildings/WizardTower.png";
            case SORCERER: return "buildings/SorcererTower.png";
            case DOUBLE_CANNON: return "buildings/DoubleCannonTower.png";
        }

        return null;
    }
}
