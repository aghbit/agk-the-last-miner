package com.to2gaming.thelastminer.phases.phaseBuilding;

/**
 * @author Mateusz Jarosz (mateusja)
 * @author Grzegorz Łaganowski (laganows)
 */

/**
 * Respond to action performed in methods that are responsible for e.g building, upgrading and deleting buildings
 * and gathering coin
 *
 * <p> SUCCESS - Performed action was succeed
 * <p> ALREADY_TAKEN - Cannot perform action, recommended space is already overtaken
 * <p> NO_GOLD - Cannot perform action, not enough amount of gold
 * <p> NO_BUILDING - Cannot perform action, no building in recommended space
 * <p> PHASE_DEACTIVATED - Cannot perform action, building phase is deactivated
 * <p> COLLECTED_COIN - Performed action of gathering gold from map was succeed
 *
 */
public enum BuildReturn {
    SUCCESS, ALREADY_TAKEN, NO_GOLD, NO_BUILDING, PHASE_DEACTIVATED, COLLECTED_COIN;
}


