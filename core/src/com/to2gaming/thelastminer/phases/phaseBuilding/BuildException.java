package com.to2gaming.thelastminer.phases.phaseBuilding;

/**
 * @author Mateusz Jarosz (mateusja)
 * @author Grzegorz Łaganowski (laganows)
 */

public class BuildException extends Exception {
    public BuildException(String message) {
      super(message);
    }
}
