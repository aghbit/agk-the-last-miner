package com.to2gaming.thelastminer.phases.phaseBuilding;

import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.utils.GdxRuntimeException;
import com.to2gaming.thelastminer.TheLastMinerGame;
import com.to2gaming.thelastminer.aimodules.buildingsai.Building;
import com.to2gaming.thelastminer.manager.MinerList;
import com.to2gaming.thelastminer.phases.interfaces.IBuildingPhase;
import com.to2gaming.thelastminer.phases.interfaces.IPhase;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.*;

/**
 * @author Mateusz Jarosz (mateusja)
 * @author Grzegorz Łaganowski (laganows)
 */

public class BuildingPhase implements IPhase, IBuildingPhase {
    private MinerList<Building> buildings;
    private ArrayList<CoinEffect> coins; //TODO: Fix probles with CoinEffect class
    private EnumMap<BuildingType, Integer> buildingsCosts = new EnumMap<>(BuildingType.class);
    private int cash;
    private int coinValue;
    private int buildingTypesNumber;

    private String configPath =  "config.properties";
    private String upgradeConfigPath = "uprade_map.properties";
    private HashMap<BuildingType, BuildingType> upgradeMap = new HashMap<>();

    private int gridWidth;
    private int gridHeight;
    private float boardHeight; //y
    private float boardWidth; //x

    private boolean active;
    private Grid grid;
    private TheLastMinerGame game;

    public BuildingPhase() {
        try {
            loadProperties();
        } catch (BuildException e) {
            e.printStackTrace();
        }
        this.grid = new Grid(gridWidth, gridHeight, boardHeight, boardWidth);
    }

    public BuildingPhase(TheLastMinerGame theLastMinerGame) {
        this.game = theLastMinerGame;
        try {
            loadProperties();
        } catch (BuildException e) {
            e.printStackTrace();
        }
        this.grid = new Grid(gridWidth, gridHeight, boardHeight, boardWidth);

    }


    /**
     * Create specified building in given area on the map. Check possibility of creating
     * the building (is building phase activated and if there is enough cash and space on the map).
     * If coin effect is available on (x, y) parameters on the map process of building doesn't happened,
     * instead of it extra cash is added to user's cash.
     * When there is another building with the same type in neighbourhood the cost of upgrade is reduced by
     * 25% for whole game time.
     *
     * <p>Dedicated for UI module.
     *
     * @param name Enum value from BuildingType (e.g. MACHINE_GUN)
     * @param x Float x position on the map
     * @param y Float y position on the map
     *
     * @return One of enum values from {@link com.to2gaming.thelastminer.phases.phaseBuilding.BuildReturn}:
     * SUCCESS, ALREADY_TAKEN, NO_GOLD, PHASE_DEACTIVATED
     */
    public BuildReturn build(BuildingType name, float x, float y) {
        if (!active) {
            return BuildReturn.PHASE_DEACTIVATED;
        }

        if (clickCoin(x, y)) {
            return BuildReturn.COLLECTED_COIN;
        }

        Building b = createInstanceOfBuilding(name, x, y);

        if (grid.checkNeighbourhood(name, x, y)) {
            b.setUpgradeMul(0.75 * b.getUpgradeMul());
        }

        if (cash >= b.getCost()) {
            if (isPlaceToBuild(x, y)) {
                cash -= b.getCost();
                buildings.add(b);
                grid.addToGrid(b, x, y);
            } else {
                return BuildReturn.ALREADY_TAKEN;
            }
        } else {
            return BuildReturn.NO_GOLD;
        }

        return BuildReturn.SUCCESS;
    }

    /**
     * Create instance of Building class form AI module which is wrapped with additional parameters
     * (upgrade cost, (x ,y) position on the map, level, building cost).
     *
     * @param type Enum value from BuildingType (e.g. MACHINE_GUN)
     * @param x Float x position on the map
     * @param y Float y position on the map
     * @return Building object from AI which is wrapped with additional parameters
     */
    private Building createInstanceOfBuilding(BuildingType type, float x, float y) {
        Building building = BuildingsFactory.getBuilding(type);
        building.setCost(buildingsCosts.get(type));

        int indexOfRow = (int) Math.floor((y / (boardHeight / gridHeight)));
        float b_y = ((float) Math.floor(indexOfRow * (boardHeight / gridHeight)));

        int indexOfColumn = (int) Math.floor((x / (boardWidth / gridWidth)));
        float b_x = ((float) Math.floor(indexOfColumn * (boardWidth / gridWidth)));

        building.setPosition(b_x, b_y);

        building.setName(type);

        //TODO For multi-field with one merged building (planned feature)
        //      building.setDimX(dimValueForXY);
        //      building.setDimY(dimValueForXY);
        return building;
    }

    /**
     * Check if area specified by float parameters (x, y) is free (True) or already taken (False).
     *
     * <p>Dedicated for UI module.
     *
     * @param x Float x position on the map
     * @param y Float y position on the map
     * @return Boolean True (no building on specified space) or False (space already overtaken)
     */
    public boolean isPlaceToBuild(float x, float y) {
        return grid.isCellFree(x, y);
    }

    /**
     * Load data for buildings (names, costs, number of different types of buildings) for config.properties
     * file form android/assets folder.
     *
     * <p>Dedicated for Manager module.
     *
     * @throws BuildException when there is no config file or when there are problem with closing it
     */
    public void loadProperties () throws BuildException {
        Properties prop = new Properties();
        InputStream input = null;

        try {
            prop.load(new FileInputStream(upgradeConfigPath));
            if (!prop.isEmpty()) {
                //upgradeMap.putAll((Map<BuildingType, BuildingType>) prop);
                for(String key : prop.stringPropertyNames()) {
                    upgradeMap.put(BuildingType.valueOf(key), BuildingType.valueOf(prop.getProperty(key)));
                }
            }

        } catch (IOException e) {
            e.printStackTrace();
        }


        try {
            input = new FileInputStream(configPath);
            prop.load(input);

            buildingTypesNumber = Integer.parseInt(prop.getProperty("number_of_buildings"));
            coinValue = Integer.parseInt(prop.getProperty("coin_value"));

            gridHeight = Integer.parseInt(prop.getProperty("grid_height"));
            gridWidth = Integer.parseInt(prop.getProperty("grid_width"));
            boardWidth = Integer.parseInt(prop.getProperty("board_width"));
            boardHeight = Integer.parseInt(prop.getProperty("board_height"));

            int tmpCost;
            String tmpName;

            for (int i = 0; i < buildingTypesNumber; i++) {
                tmpCost = Integer.parseInt(prop.getProperty("building_cost_" + i));
                tmpName = (prop.getProperty("building_name_" + i));

                buildingsCosts.put(BuildingType.valueOf(tmpName), tmpCost);
            }

        } catch (IOException e) {
            throw new BuildException("No config file!");
        } finally {
            if (input != null) {
                try {
                    input.close();
                } catch (IOException e) {
                    throw new BuildException("Cannot close config file!");
                }
            }
        }
    }

    /**
     * Returns list of enums (BuildType) with building types that are possible to build with current amount of cash.
     *
     * <p>Dedicated for UI module.
     *
     * @return MinerList&lt;BuildingType&gt; with building types
     */
    public ArrayList<Building> getAvailableBuildingList() {
        ArrayList<Building> availableBuildingTypes = new ArrayList<>();

        for (Map.Entry<BuildingType, Integer> entry : buildingsCosts.entrySet()) {
            int valueCost = entry.getValue();
            //fixme DLACZEMU??? :<
            //if (valueCost <= cash) {
                Building b = BuildingsFactory.getBuilding(entry.getKey());
            if(b != null)
                availableBuildingTypes.add(b);
            //}
        }

        return availableBuildingTypes;
    }

    /**
     * Upgrade building in specified space by float parameters (x, y) as position on the map. Check
     * if building phase is activated, amount of cash and presence of any building on (x, y) position.
     * If coin effect is available on (x, y) parameters on the map process of upgrading doesn't happened,
     * instead of it extra cash is added to user's cash.
     *
     * <p>Dedicated for UI module.
     *
     * @param x Float x position on the map
     * @param y Float y position on the map
     *
     * @return One of enum values from {@link com.to2gaming.thelastminer.phases.phaseBuilding.BuildReturn}:
     * SUCCESS, NO_BUILDING, NO_GOLD, PHASE_DEACTIVATED
     */
    public BuildReturn upgradeBuilding(float x, float y) {
        if (!active) {
            return BuildReturn.PHASE_DEACTIVATED;
        }
        if (clickCoin(x,y)) {
            return BuildReturn.COLLECTED_COIN;
        }
        if (!grid.isCellFree(x, y)) {
            Building b = grid.getGridCell(x, y);
            int cost = b.getCost();
            if (cash >= cost * b.getLevel() * b.getUpgradeMul()) {
                cash -= cost;
                //b.setLevel(b.getLevel() + 1);
                BuildingType tmpBuildingType = upgradeMap.get(b.getBuildingName());
                if (tmpBuildingType != null && destroyBuilding(x,y) == BuildReturn.SUCCESS) {
                    Building upgradedBuilding = createInstanceOfBuilding(tmpBuildingType, x, y);
                    buildings.add(upgradedBuilding);
                    grid.addToGrid(upgradedBuilding, x, y);
                }

            }
            else {
                return BuildReturn.NO_GOLD;
            }
        }
        else {
            return BuildReturn.NO_BUILDING;
        }
        return BuildReturn.SUCCESS;
    }

    /**
     * Delete building in specified space by float parameters (x, y) as position on the map. Check
     * if building phase is activated and presence of any building on (x, y) position.
     * If coin effect is available on (x, y) parameters on the map process of deleting doesn't happened,
     * instead of it extra cash is added to user's cash.
     *
     * <p>Dedicated for UI module.
     *
     * @param x Float x position on the map
     * @param y Float y position on the map
     *
     * @return One of enum values from {@link com.to2gaming.thelastminer.phases.phaseBuilding.BuildReturn}:
     * SUCCESS, NO_BUILDING, PHASE_DEACTIVATED
     */
    public BuildReturn destroyBuilding(float x, float y) {
        if (!active){
            return BuildReturn.PHASE_DEACTIVATED;
        }

        if (clickCoin(x,y)) {
            return BuildReturn.COLLECTED_COIN;
        }

        if (!grid.isCellFree(x, y)){
            Building b = grid.getGridCell(x,y);
            b.destroyActor();
            cash += b.getCost();
            buildings.remove(b);
            grid.deleteFromGrid(x, y);
        }
        else {
            return BuildReturn.NO_BUILDING;
        }
        return BuildReturn.SUCCESS;
    }

    /**
     * Create random number of coins on the map with beginning of building phase
     * (included also in {@link com.to2gaming.thelastminer.phases.phaseBuilding.BuildingPhase#activate()} method.)
     *
     * <p>Dedicated for Manager module.
     *
     */
    public void createCoins() {
        Random generator = new Random();
        coins = new ArrayList<CoinEffect>();
        int coinsToGenerate = 6;//generator.nextInt(7) + 1;
        while (coinsToGenerate > 0) {
            int x = 50 + generator.nextInt((int) boardWidth - 300);
            int y = generator.nextInt((int) boardHeight);
            CoinEffect coin = new CoinEffect(x, y, game);
            game.addEffect(coin);
            coins.add(coin);
            coinsToGenerate--;
        }
    }

    /**
     * Handle execution of clicking in specified area in order to gain addition cash from coin object on the map.
     *
     * <p>Dedicated for UI module.
     *
     * @param x Float x position on the map
     * @param y Float y position on the map
     *
     */
    public boolean clickCoin(float x, float y){
        CoinEffect coin = findCoin(x, y, coins);
        if (coin != null){
            coins.remove(coin);
            game.removeEffect(coin);
            coin.destroyActor();
            cash += coinValue;
            return true;
        }
        return false;
    }


    /**
     * Search coin in recommended space.
     *
     * @param x Float x position on the map
     * @param y Float y position on the map
     * @param coins List<Building> coins
     *
     * @return Object of coin when managed to find or null when there is no coin in specified space
     *
     */
    private CoinEffect findCoin(float x, float y, List<CoinEffect> coins) {
        for (CoinEffect c : coins) {
            if ((c.getX() <= x && c.getX() + 15 >= x) || (c.getY()<= y && c.getY()+ 16 >= y)) {
                return c;
            }
        }
        return null;
    }

    public void cleanGrid() {
        boolean f = false;
        for (int i = 0; i < gridWidth - 1; i++) {
            for (int j = 0; j < gridHeight - 1; j++ ) {
                if (grid.getGridCell(i * grid.getCellWidth(),j* grid.getCellHeight()) != null) {
                    for (Building b : buildings) {
                        if (b==grid.getGridCell(i * grid.getCellWidth(),j* grid.getCellHeight())) {
                            f = true;
                        }
                    }
                    if (false == f){
                        grid.deleteFromGrid(i * grid.getCellWidth(), j * grid.getCellHeight());
                    }
                    f = false;
                }
            }
        }
    }


    //////////////////////////////////
    ////    settery / gettery      ///
    //////////////////////////////////

    public void setCash(int cash) {
        this.cash = cash;
    }

    public int getCash() {
        return cash;
    }

    public ArrayList<CoinEffect> getCoins(){
        return coins;
    }

    public void setBuildings(MinerList<Building> buildings){
        this.buildings=buildings;
    }

    public MinerList<Building> getBuildings() {
        return buildings;
    }

    public EnumMap<BuildingType, Integer> getBuildingsCosts() {
        return buildingsCosts;
    }

    public int getBuildingTypesNumber() {
        return buildingTypesNumber;
    }

    public Grid getGrid() {
        return grid;
    }



    //////////////////////////////////
    //////////////////////////////////
    //////////////////////////////////

    @Override
    public void update() {

    }

    @Override
    public void activate() {
        cleanGrid();
        this.active = true;
        createCoins();
    }

    @Override
    public void deactivate() {
        this.active = false;
    }


    public void setNewData(MinerList<Building> buildingList, int cash) {
        this.buildings = buildingList; //TODO  Manager module should delete buildings also from grid!!!
        this.cash = cash;
    }

}
