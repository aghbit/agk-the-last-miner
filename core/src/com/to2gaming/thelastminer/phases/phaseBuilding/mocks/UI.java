package com.to2gaming.thelastminer.phases.phaseBuilding.mocks;
import com.to2gaming.thelastminer.phases.phaseBuilding.BuildingPhase;

/**
 * @author Mateusz Jarosz (mateusja)
 * @author Grzegorz Łaganowski (laganows)
 */

public class UI {
    private BuildingPhase phaseModule;


    public BuildingPhase getPhaseModule() {
        return phaseModule;
    }
}
