package com.to2gaming.thelastminer.phases.phaseBuilding.mocks;
import com.to2gaming.thelastminer.phases.phaseBuilding.BuildingType;

/**
 * @author Mateusz Jarosz (mateusja)
 * @author Grzegorz Łaganowski (laganows)
 */

public class BuildingsFactory {

    public static Building getBuilding(BuildingType name) {
        return new Building();
    }

    public Building getBuilding() {
         return new Building();
    }
}
