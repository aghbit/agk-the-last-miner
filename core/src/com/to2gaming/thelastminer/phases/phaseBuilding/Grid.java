package com.to2gaming.thelastminer.phases.phaseBuilding;


import com.to2gaming.thelastminer.aimodules.buildingsai.Building;

/**
 * @author Mateusz Jarosz (mateusja)
 * @author Grzegorz Łaganowski (laganows)
 */
//TODO add javadoc

public class Grid {
    private int x;
    private int y;

    private float boardHeight;
    private float boardWidth;

    private float cellHeight;
    private float cellWidth;

    private Building[][] gridArray;

    public Grid (int x, int y, float boardHeight, float boardWidth) {
        this.boardHeight = boardHeight;
        this.boardWidth = boardWidth;
        this.x = x;
        this.y = y;
        this.cellHeight = boardHeight / y;
        this.cellWidth = boardWidth / x;
        this.gridArray = new Building[y][x];
        for (int i = 0; i < x; i++) {
            for (int j = 0; j < y; j++) {
                gridArray[j][i] = null;
            }
        }
    }

    public BuildReturn addToGrid(Building b, float x, float y) {
        if (isCellFree(x, y)) {
            gridArray[(int)Math.floor(y/cellHeight)][(int)Math.floor(x/cellWidth)] = b;
            return BuildReturn.SUCCESS;
        } else {
            return BuildReturn.ALREADY_TAKEN;
        }
    }

    public BuildReturn deleteFromGrid(float x, float y) {
        if (!isCellFree(x, y)) {
            gridArray[(int)Math.floor(y/cellHeight)][(int)Math.floor(x/cellWidth)] = null;
            return BuildReturn.SUCCESS;
        } else {
            return BuildReturn.NO_BUILDING;
        }
    }

    public boolean isCellFree(float x, float y) {
        try {
            if (((int)Math.floor(y/cellHeight) < (this.y/2 + 1)) && ((int)Math.floor(y/cellHeight) > (this.y/2 - 2))) {
                if (((int)Math.floor(x/cellWidth) < (this.x/2 + 2)) && ((int)Math.floor(x/cellWidth) > (this.x/2 - 1)))
                    return false;
            }


            if (gridArray[(int)Math.floor(y/cellHeight)][(int)Math.floor(x/cellWidth)] == null) {
                return true;
            } else {
                return false;
            }
        } catch (ArrayIndexOutOfBoundsException e) {
            return false;
        }
    }

    public Building getGridCell(float x, float y) {
        return gridArray[(int)Math.floor(y/cellHeight)][(int)Math.floor(x/cellWidth)];
    }

    public boolean checkNeighbourhood(BuildingType name, float x, float y) {
        int indexOfRow = (int) Math.floor((y / cellHeight));
        int indexOfColumn = (int) Math.floor((x / cellWidth));

        int[] xTab = new int[]{-1, 1, -1, 0, 1, -1, 0, 1};
        int[] yTab = new int[]{0, 0, 1, 1, 1, -1, -1, -1};

        for (int i = 0; i < 8; i++) {
            if ((indexOfRow + yTab[i] < 0 || indexOfRow + yTab[i] >= this.y-1 ) ||
                    (indexOfColumn + xTab[i] < 0 || indexOfColumn + xTab[i] >= this.x-1 ))
                continue;
            if (gridArray[indexOfRow + yTab[i]][indexOfColumn + xTab[i]] == null) {
                continue;
            }
            if (gridArray[indexOfRow + yTab[i]][indexOfColumn + xTab[i]].getBuildingName() == name) {
                return true;
            }

        }
        return false;
    }

    public float getCellWidth() {
        return cellWidth;
    }

    public float getCellHeight() {
        return cellHeight;
    }

    public float getBoardWidth() {
        return boardWidth;
    }

    public float getBoardHeight() {
        return boardHeight;
    }
}
