package com.to2gaming.thelastminer.phases.phaseBuilding;

import com.badlogic.gdx.graphics.Color;
import com.to2gaming.thelastminer.TheLastMinerGame;
import com.to2gaming.thelastminer.ui.effects.SimpleEffect;
import com.to2gaming.thelastminer.ui.util.UIConstants;

/**
 * @author Mateusz Jarosz (mateusja)
 * @author Grzegorz Łaganowski (laganows)
 */

public class CoinEffect extends SimpleEffect {
    private float life = UIConstants.COIN_LIFE;
    private TheLastMinerGame game;

    public CoinEffect(float x, float y, TheLastMinerGame game) {
        super("coinEffect.png", x, y, 2, game);
        getActor().setColor(Color.YELLOW);
        this.game = game;
    }

    @Override
    public void update(float delta) {
        life -= delta;
        if (life <= 0) {
            game.removeEffect(this);
            destroyActor();
        }
    }
}
