package com.to2gaming.thelastminer.phases.phaseBuilding;

public enum BuildingType {
    MACHINE_GUN("MACHINE_GUN"),
    TWO_MACHINE_GUN("TWO_MACHINE_GUN"),
    CANNON("CANNON"),
    DOUBLE_CANNON("DOUBLE_CANNON"),
    WIZARD("WIZARD"),
    SORCERER("SORCERER"),
    MINE("MINE");

    private String type;

    BuildingType(String type) {
        this.type = type;
    }

    @Override
    public String toString() {
        return type;
    }
}
