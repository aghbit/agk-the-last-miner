package com.to2gaming.thelastminer.phases.interfaces;

/**
 * Created by haz111 on 26.11.15.
 */
public interface IPhase {

    void update();
    void activate();
    void deactivate();

}
