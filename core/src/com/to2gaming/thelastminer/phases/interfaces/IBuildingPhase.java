package com.to2gaming.thelastminer.phases.interfaces;

import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.to2gaming.thelastminer.aimodules.buildingsai.Building;
import com.to2gaming.thelastminer.manager.MinerList;
import com.to2gaming.thelastminer.phases.phaseBuilding.BuildException;
import com.to2gaming.thelastminer.phases.phaseBuilding.BuildReturn;
import com.to2gaming.thelastminer.phases.phaseBuilding.BuildingType;
import com.to2gaming.thelastminer.phases.phaseBuilding.CoinEffect;


import java.io.IOException;
import java.util.ArrayList;

/**
 * @author Mateusz Jarosz (mateusja)
 * @author Grzegorz Łaganowski (laganows)
 */

public interface IBuildingPhase {
    public BuildReturn build(BuildingType name, float x, float y) throws BuildException;
    public ArrayList<Building> getAvailableBuildingList();
    public BuildReturn upgradeBuilding(float x, float y) throws BuildException;
    public BuildReturn destroyBuilding(float x, float y) throws BuildException;
    public ArrayList<CoinEffect> getCoins();
    public void createCoins();
    public boolean clickCoin(float x, float y);
    public boolean isPlaceToBuild(float x, float y);
    public void loadProperties() throws BuildException;
}