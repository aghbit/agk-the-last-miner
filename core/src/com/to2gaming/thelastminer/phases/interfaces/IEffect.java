package com.to2gaming.thelastminer.phases.interfaces;
import com.badlogic.gdx.scenes.scene2d.ui.Image;

/**
 * @author Paweł Białas
 * @author Paweł Ćwik
 */

public interface IEffect {
    void update(float delta);
    Image getImage();
}
