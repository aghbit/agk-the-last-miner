package com.to2gaming.thelastminer.manager;

import com.badlogic.gdx.math.Vector2;
import com.to2gaming.thelastminer.aimodules.enemyai.Enemy;
import com.to2gaming.thelastminer.aimodules.enemyai.enemies.BasicEnemy;
import com.to2gaming.thelastminer.aimodules.enemyai.enemies.BasicEnemy2;
import com.to2gaming.thelastminer.aimodules.enemyai.enemies.BasicEnemy3;
import com.to2gaming.thelastminer.aimodules.enemyai.enemies.EnemyType;
import com.to2gaming.thelastminer.ui.util.UIConstants;

import java.util.Random;

/**
 * Created by haz111 on 29.11.15.
 */
public final class EnemyGenerator {
    private static final float heightC = UIConstants.SCREEN_HEIGHT;
    private static final float widthC = UIConstants.SCREEN_WIDTH;

    public static Enemy generateEnemy(int level) {
        Random rand = new Random();
        int n = rand.nextInt(3);
        EnemyType enemyType = EnemyType.values()[n];
        Vector2 v = getPosition();
        if (enemyType.equals(EnemyType.BASIC_ENEMY)) {
            return new BasicEnemy(v.x, v.y);
        } else if (enemyType.equals(EnemyType.BASIC_ENEMY2)) {
            return new BasicEnemy2(v.x, v.y);
        } else if (enemyType.equals(EnemyType.BASIC_ENEMY3)) {
            return new BasicEnemy3(v.x, v.y);
        } else{

            return null;
        }
    }


    private static Vector2 getPosition() {
        Random rand = new Random();
        Vector2 result = new Vector2();
        switch(rand.nextInt(4)){
            case (0):
                result.set(rand.nextFloat() * widthC, heightC);
                break;
            case (1):
                result.set(widthC, rand.nextFloat() * heightC);
                break;
            case (2):
                result.set(rand.nextFloat() * widthC, 0f);
                break;
            case (3):
                result.set(0f, rand.nextFloat() * heightC);
                break;
            default:
                result.set(0f, 0f);
        }
        return result;
    }
}
