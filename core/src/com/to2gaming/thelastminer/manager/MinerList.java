package com.to2gaming.thelastminer.manager;

import com.to2gaming.thelastminer.aimodules.interfaces.GameObject;
import com.to2gaming.thelastminer.ui.util.UIConstants;

import java.util.ArrayList;

/**
 * Created by Tymoteusz on 15.11.2015.
 */
public class MinerList<E extends GameObject> extends ArrayList<E> {


    //FIXME: prosta i niewydajna implementacja
    public MinerList<E> getElementsInRange(float posX, float posY, float range) {
        MinerList<E> resultList = new MinerList<>();
        for(E actor : this) {
            double distance = Math.sqrt(Math.abs(Math.pow(posX - actor.getX(), 2f) + Math.pow(posY - actor.getY(), 2f)));
            if(distance < range) {
                resultList.add(actor);
            }
        }
        return resultList;
    }

    //FIXME: zrobic lepiej
    public boolean anyoneInTheMiddle() {
        int middleX = UIConstants.SCREEN_WIDTH / 2;
        int middleY = UIConstants.SCREEN_HEIGHT / 2;
        for(E actor : this) {
            if(Math.abs(actor.getX()-middleX) <= 40  && Math.abs(actor.getY()-middleY) <= 40)
                return true;
        }
        return false;
    }
}
