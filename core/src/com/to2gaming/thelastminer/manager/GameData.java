package com.to2gaming.thelastminer.manager;


import com.to2gaming.thelastminer.aimodules.enemyai.Enemy;
import com.to2gaming.thelastminer.other.spells.SpellType;
import com.to2gaming.thelastminer.aimodules.buildingsai.Building;
import com.to2gaming.thelastminer.phases.phaseBuilding.BuildingType;
import com.to2gaming.thelastminer.ui.effects.Effect;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.CopyOnWriteArrayList;

public class GameData {
    MinerList<Building> buildingList = new MinerList<Building>();
    MinerList<Enemy> enemyList = new MinerList<Enemy>();
    //FIXME czy to tak ma byc, ze MinerList jest tylko dla AI?
    List<Effect> effectList = new CopyOnWriteArrayList<Effect>();
    int cash;
    int level;
    String nick;
    Difficulty difficulty;
    Set<BuildingType> unlockedBuildings;
    Set<SpellType> skills;
    int mana;

    public void generateEnemies() {
        enemyList.clear();
        for (int i = 0; i < 4 * level + 2; i++) {
            enemyList.add(EnemyGenerator.generateEnemy(level));
        }
    }

    public MinerList<Building> getBuildingList() {
        return buildingList;
    }

    public void setBuildingList(MinerList<Building> buildingList) {
        this.buildingList = buildingList;
    }

    public MinerList<Enemy> getEnemyList() {
        return enemyList;
    }

    public void setEnemyList(MinerList<Enemy> enemyList) {
        this.enemyList = enemyList;
    }

    public List<Effect> getEffectList() { return effectList; }

    public void setEffectList(List<Effect> effectList) { this.effectList = effectList; }

    public int getCash() {
        return cash;
    }

    public void setCash(int cash) {
        this.cash = cash;
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public String getNick() {
        return nick;
    }

    public void setNick(String nick) {
        this.nick = nick;
    }

    public Difficulty getDifficulty() {
        return difficulty;
    }

    public void setDifficulty(Difficulty difficulty) {
        this.difficulty = difficulty;
    }

    public Set<SpellType> getSkills() {
        return skills;
    }

    public Set<BuildingType> getUnlockedBuildings() {
        return unlockedBuildings;
    }

    public int getMana() {
        return mana;
    }

    public void setMana(int mana) {
        this.mana = mana;
    }

    public void updateEffects(float deltaTime) {
        for(Effect e : effectList) {
            e.update(deltaTime);
        }
    }
}