package com.to2gaming.thelastminer.manager;

/**
 * Created by Tymoteusz on 15.11.2015.
 */
public enum GamePhase {
    START, BUILDING, FIGHTING, GAME_OVER
}
