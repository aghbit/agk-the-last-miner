package com.to2gaming.thelastminer.manager.comparators;

import com.to2gaming.thelastminer.aimodules.interfaces.GameObject;

import java.util.Comparator;

public class ComparatorByHealth implements Comparator<GameObject> {
    @Override
    public int compare(GameObject o1, GameObject o2) {
        return o2.getHp() - o1.getHp();
    }
}
