package com.to2gaming.thelastminer.manager;

/**
 * Created by Tymoteusz on 15.11.2015.
 */
public enum Difficulty {
    EASY, MEDIUM, HARD
}
