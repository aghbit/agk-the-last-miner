package com.to2gaming.thelastminer.ui.screens;

import com.to2gaming.thelastminer.TheLastMinerGame;
import com.to2gaming.thelastminer.ui.stages.DeadStage;

/**
 * Created by Sylwia Grzeszczak on 18.01.16.
 */
public class DeadScreen extends MinerScreen {
    public DeadScreen(TheLastMinerGame game) { stage = new DeadStage(game); }
}
