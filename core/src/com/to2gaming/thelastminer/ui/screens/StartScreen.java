package com.to2gaming.thelastminer.ui.screens;

import com.to2gaming.thelastminer.TheLastMinerGame;
import com.to2gaming.thelastminer.ui.stages.StartStage;

public class StartScreen extends MinerScreen {
    public StartScreen(TheLastMinerGame game) {
        stage = new StartStage(game);
    }
}
