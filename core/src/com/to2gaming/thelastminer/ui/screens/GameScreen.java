package com.to2gaming.thelastminer.ui.screens;

import com.to2gaming.thelastminer.TheLastMinerGame;
import com.to2gaming.thelastminer.manager.GamePhase;
import com.to2gaming.thelastminer.phases.phaseBuilding.BuildingPhase;
import com.to2gaming.thelastminer.phases.phaseDefence.DefencePhase;
import com.to2gaming.thelastminer.ui.stages.GameStage;

public class GameScreen extends MinerScreen {
    public GameScreen(BuildingPhase buildingPhase, DefencePhase defencePhase, TheLastMinerGame game) {
        stage = new GameStage(buildingPhase, defencePhase, game);
    }

    @Override
    public void phaseChanged(GamePhase phase) {
        ((GameStage) stage).phaseChanged(phase);
    }
}
