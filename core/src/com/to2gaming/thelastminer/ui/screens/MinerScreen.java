package com.to2gaming.thelastminer.ui.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.ScreenAdapter;
import com.badlogic.gdx.graphics.GL20;
import com.to2gaming.thelastminer.manager.GameData;
import com.to2gaming.thelastminer.manager.GamePhase;
import com.to2gaming.thelastminer.ui.stages.MinerStage;

public abstract class MinerScreen extends ScreenAdapter {
    protected MinerStage stage;
    protected GameData gameData;

    public MinerStage getStage() {
        return stage;
    }

    @Override
    public void show() {
    }

    public void setGameData(GameData gameData) {
        this.gameData = gameData;
        stage.setGameData(gameData);
    }

    public void phaseChanged(GamePhase phase) {
    }

    @Override
    public void render(float delta) {
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        Gdx.input.setInputProcessor(stage);
        stage.draw();
        //stage.act(delta);
    }

    @Override
    public void resize(int width, int height) {
    }

    @Override
    public void pause() {
    }

    @Override
    public void resume() {
    }

    @Override
    public void hide() {
    }

    @Override
    public void dispose() {
    }
}
