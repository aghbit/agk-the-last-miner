package com.to2gaming.thelastminer.ui;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.to2gaming.thelastminer.ui.util.AnimatedImage;
import com.to2gaming.thelastminer.ui.util.UIConstants;
import com.to2gaming.thelastminer.ui.util.UILogger;

/**
 * Objects that are supposed to be physically visible should extend this class.
 * Example object:
 * MyObject extends ObjectWithActor {
 *    public MyObject() {
 *        setImageAsActor(); //may be also setAnimationAsActor, copyActor
 *    }
 *    public void die() {
 *        destroyActor();
 *    }
 * }
 */
public abstract class ObjectWithActor {
    /**
     * Actor is something that will be displayed.
     * An UI representation for current GameObject.
     */
    protected Actor actor;

    /**
     * used for setting layer
     * INTERFACE is above all
     * BUILDING < ENEMY < EFFECT
     */
    public enum LayerType {
        INTERFACE, ENEMY, BUILDING, EFFECT
    }

    /**
     * Meant for advanced Actor handling
     *
     * @return instance of Actor that is bound to this object
     */
    public Actor getActor() {
        return actor;
    }

    /**
     * Sets up a new Actor for current object
     *
     * @param path path to the image
     */
    protected void setImageAsActor(String path) {
        actor = new Image(new Texture(path));
    }

    /**
     * Sets up a new animated Actor for current object
     *
     * @param path path to animation atlas //TODO tutorial/example
     * @param fps  frames per second for animation
     */
    protected void setAnimationAsActor(String path, float fps) {
        TextureAtlas atlas = new TextureAtlas(path);
        Animation animation = new Animation(fps, atlas.getRegions());
        actor = new AnimatedImage(animation);
    }

    /**
     * Sets up a copy of existing Actor as this object's Actor
     * @param another    the source of copy
     */
    protected void copyActor(ObjectWithActor another) {
        if(another.actor instanceof AnimatedImage) {
            actor = ((AnimatedImage)another.actor).copy();
        } else if (another.actor instanceof Image){
            actor = new Image(((Image) another.actor).getDrawable());
        } else {
            UILogger.log("ObjectWithActor.copyActor(another): unsupported another.actor");
        }
    }

    /**
     * Sets actor in new position.
     *
     * @param x distance to left margin
     * @param y distance to bottom margin
     */
    public void setPosition(float x, float y) {
        actor.setPosition(x, y);
    }

    /**
     * Sets the z-index of actor.
     * default layer = 0
     * effective layer = 4*layer + layerType
     * maximum layer and interface layer are defined in UIConstants
     *
     * @param layer     how high is the layer
     * @param layerType id of layer type (ObjectWithActor.LayerType)
     */
    protected void setLayer(LayerType layerType, int layer) {
        int ltype;
        switch (layerType) {
            case BUILDING:
                ltype = 0;
                break;
            case ENEMY:
                ltype = 1;
                break;
            case EFFECT:
                ltype = 2;
                break;
            case INTERFACE:
            default:
                actor.setZIndex(UIConstants.INTERFACE_LAYER);
                return;
        }
        ltype = 4 * layer + ltype;
        if (ltype > UIConstants.MAXIMUM_LAYER)
            ltype = UIConstants.MAXIMUM_LAYER;
        actor.setZIndex(ltype);
    }

    /**
     * Should be called when actor is supposed to disappear.
     */
    public void destroyActor() {
        actor.remove();
    }

    /**
     * Setting actor's speed
     *
     * @param x horizontal
     * @param y vertical
     */
    public void setActorSpeed(float x, float y) {
        //TODO
    }

    public float getX() {
        return actor.getX();
    }

    public float getY() {
        return actor.getY();
    }
}
