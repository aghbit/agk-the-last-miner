package com.to2gaming.thelastminer.ui.effects;

import com.to2gaming.thelastminer.ui.ObjectWithActor;

/**
 * Created by TWOJA STARA on 21.12.15.
 */
public abstract class Effect extends ObjectWithActor {
    public abstract void update(float delta);
}
