package com.to2gaming.thelastminer.ui.effects;

import com.to2gaming.thelastminer.TheLastMinerGame;

/**
 * Created by Andrzej Duda on 21.12.15.
 */
public abstract class SimpleEffect extends Effect {
    private float life;
    private TheLastMinerGame game;

    public SimpleEffect(String pathToImage, float x, float y, float life, TheLastMinerGame game) {
        setImageAsActor(pathToImage);
        setPosition(x, y);
        this.life = life;
        this.game = game;
    }

    public SimpleEffect(String pathToAnimation, float fps, float x, float y, float life, TheLastMinerGame game) {
        setAnimationAsActor(pathToAnimation, fps);
        setPosition(x, y);
        this.life = life;
        this.game = game;
    }

    @Override
    public void update(float delta) {
        life -= delta;
        if(life < 0) {
            game.removeEffect(this);
            destroyActor();
        }
    }
}
