package com.to2gaming.thelastminer.ui.actors;

import com.badlogic.gdx.graphics.Color;
import com.to2gaming.thelastminer.TheLastMinerGame;
import com.to2gaming.thelastminer.aimodules.buildingsai.Building;
import com.to2gaming.thelastminer.phases.phaseBuilding.BuildingType;
import com.to2gaming.thelastminer.ui.effects.SimpleEffect;
import com.to2gaming.thelastminer.ui.util.UIConstants;
import com.to2gaming.thelastminer.ui.effects.Effect;

public class BuildFailedEffect extends Effect {
    private float life = UIConstants.BUILD_FAILED_LIFE;
    private TheLastMinerGame game;

    public BuildFailedEffect(Building building, float x, float y, TheLastMinerGame game) {
        copyActor(building);
        getActor().setColor(Color.RED);
        setPosition(x, y);
        this.game = game;
    }

    @Override
    public void update(float delta) {
        life -= delta;
        if (life <= 0) {
            game.removeEffect(this);
            destroyActor();
        }
    }
}