package com.to2gaming.thelastminer.ui.actors;

import com.badlogic.gdx.Gdx;
import com.to2gaming.thelastminer.ui.ObjectWithActor;
import com.to2gaming.thelastminer.ui.util.UIConstants;


public class Avatar extends ObjectWithActor {
    public Avatar() {
        setImageAsActor("gornik2.png");
        setPosition(Gdx.graphics.getWidth()/2, Gdx.graphics.getHeight()/2);
        getActor().setWidth(UIConstants.AVATAR_WIDTH);
        getActor().setHeight(UIConstants.AVATAR_HEIGHT);
    }
}
