package com.to2gaming.thelastminer.ui.stages;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.to2gaming.thelastminer.TheLastMinerGame;
import com.to2gaming.thelastminer.aimodules.enemyai.Enemy;
import com.to2gaming.thelastminer.aimodules.buildingsai.Building;
import com.to2gaming.thelastminer.manager.GameData;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.to2gaming.thelastminer.manager.GamePhase;
import com.to2gaming.thelastminer.other.spells.Spell;
import com.to2gaming.thelastminer.other.spells.SpellConfig;
import com.to2gaming.thelastminer.other.spells.SpellType;
import com.to2gaming.thelastminer.phases.phaseBuilding.BuildReturn;
import com.to2gaming.thelastminer.phases.phaseBuilding.BuildingPhase;
import com.to2gaming.thelastminer.phases.phaseBuilding.BuildingType;
import com.to2gaming.thelastminer.phases.phaseDefence.DefencePhase;
import com.to2gaming.thelastminer.ui.ObjectWithActor;
import com.to2gaming.thelastminer.ui.actors.Avatar;
import com.to2gaming.thelastminer.ui.effects.Effect;
import com.to2gaming.thelastminer.ui.util.UIConstants;
import com.to2gaming.thelastminer.ui.actors.BuildFailedEffect;
import com.to2gaming.thelastminer.ui.util.UILogger;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class GameStage extends MinerStage {
    private GamePhase newPhase;
    private GamePhase lastPhase;
    private BuildingPhase buildingPhase;
    private DefencePhase defencePhase;
    private TheLastMinerGame game;

    private Building selectedBuilding;
    private SpellType selectedSpell;
    private Mode selectedMode;

    private enum Mode {
        BUILDING, //selected a building to build
        CASTING, //selected a spell to cast
        NONE, //nope, nothing
        DESTROY, //clicked Mighty Destroy Button(c)
        UPGRADE //clicked Not-So-Mighty Upgrade Button(c)
    }

    private Rectangle buildingList = new Rectangle(0, 0, 0, 0);
    private Map<Rectangle, Building> buildingListMap = new HashMap<>();
    private Rectangle spellList = new Rectangle(0, 0, 0, 0);
    private Rectangle destroyButton = new Rectangle(0, 0, 0, 0);
    private Rectangle upgradeButton = new Rectangle(0, 0, 0, 0);
    private Map<Rectangle, SpellType> spellListMap = new HashMap<>();
    private Vector3 touchPoint;

    private Image background;
    private Image destroyButtonImage, upgradeButtonImage;
    private Avatar avatar;

    public GameStage(BuildingPhase buildingPhase, DefencePhase defencePhase, TheLastMinerGame game) {
        super();
        selectedMode = Mode.NONE;
        this.buildingPhase = buildingPhase;
        this.defencePhase = defencePhase;
        this.game = game;
        lastPhase = GamePhase.FIGHTING;
        newPhase = GamePhase.BUILDING;
        background = new Image(new Texture(UIConstants.BG_DAY));
        destroyButtonImage = new Image(new Texture(UIConstants.BUTTON_DESTROY));
        upgradeButtonImage = new Image(new Texture(UIConstants.BUTTON_UPGRADE));
        avatar = new Avatar();
        //TODO addActor(new Avatar());
    }

    @Override
    public boolean touchDown(int x, int y, int pointer, int button) {
        touchPoint = new Vector3(x, y, 0);
        translateScreenToWorldCoordinates(x,y);
        if (buildingList.contains(touchPoint.x, touchPoint.y))
            checkBuildingListTouch();
        else if (spellList.contains(touchPoint.x, touchPoint.y))
            checkSpellListTouch();
        else if (destroyButton.contains(touchPoint.x, touchPoint.y))
            selectedMode = Mode.DESTROY;
        else if (upgradeButton.contains(touchPoint.x, touchPoint.y))
            selectedMode = Mode.UPGRADE;
        else if (x >= getButtonWidth() &&  UIConstants.SCREEN_WIDTH - getButtonWidth() >= x) // u shall not pass ~~Interface the Grey
            checkModeExecution();
        return super.touchDown(x, y, pointer, button);
    }

    private void translateScreenToWorldCoordinates(int x, int y) {
        getCamera().unproject(touchPoint.set(x, y, 0));
    }

    private void checkBuildingListTouch() {
        for (Map.Entry<Rectangle, Building> i : buildingListMap.entrySet())
            if (i.getKey().contains(touchPoint.x, touchPoint.y)) {
                selectedBuilding = i.getValue();
                selectedMode = Mode.BUILDING;
                return;
            }
    }

    private void checkSpellListTouch() {
        for (Map.Entry<Rectangle, SpellType> i : spellListMap.entrySet())
            if (i.getKey().contains(touchPoint.x, touchPoint.y)) {
                selectedSpell = i.getValue();
                selectedMode = Mode.CASTING;
                return;
            }
    }

    private void checkModeExecution() {
        switch (selectedMode) {
            case BUILDING: {
                if (lastPhase == GamePhase.BUILDING) {
                    BuildReturn br = buildingPhase.build(selectedBuilding.getBuildingName(), touchPoint.x, touchPoint.y);
                    UILogger.log("build result: " + br.name());
                    switch (br) {
                        case ALREADY_TAKEN:
                        case NO_GOLD:
                            game.addEffect(new BuildFailedEffect(selectedBuilding, touchPoint.x, touchPoint.y, game));
                            break;
                        case NO_BUILDING:
                        case SUCCESS:
                        case PHASE_DEACTIVATED:
                        case COLLECTED_COIN:
                        default:
                            break;
                    }
                }
                break;
            }
            case UPGRADE: {
                if (lastPhase == GamePhase.BUILDING)
                    buildingPhase.upgradeBuilding(touchPoint.x, touchPoint.y);
                break;
            }
            case DESTROY: {
                if (lastPhase == GamePhase.BUILDING)
                    buildingPhase.destroyBuilding(touchPoint.x, touchPoint.y);
                break;
            }
            case CASTING: {
                if (lastPhase == GamePhase.FIGHTING)
                    defencePhase.useSpell(selectedSpell, touchPoint.x, touchPoint.y);
            }
            default : {
                if (lastPhase == GamePhase.BUILDING)
                    buildingPhase.clickCoin(touchPoint.x, touchPoint.y);
                break;
            }
        }
    }

    @Override
    public void setGameData(GameData gameData) {
        if (newPhase != lastPhase) {
            background.remove();
            if (newPhase == GamePhase.BUILDING) {
                background = new Image(new Texture(UIConstants.BG_DAY));
                addActor(background);
                for(Map.Entry<Rectangle, SpellType> i : spellListMap.entrySet())
                    i.getValue().destroyActor();
                spellList = new Rectangle(0,0,0,0);
                buildingList = new Rectangle(0, 2*getButtonHeight(), getButtonWidth(), getButtonHeight() * (2 + buildingPhase.getAvailableBuildingList().size()));
                buildingListMap = new HashMap<>();
                int i = 2;
                //if (buildingPhase.getAvailableBuildingList().isEmpty())
                //    UILogger.log("[WW][GameStage]Building list is empty!");
                for(Building b : buildingPhase.getAvailableBuildingList()) {
                    b.getActor().setHeight(getButtonHeight());
                    b.getActor().setWidth(getButtonWidth());
                    b.setPosition(0, i * getButtonHeight());
                    buildingListMap.put(new Rectangle(0, i * getButtonHeight(), getButtonWidth(), getButtonHeight()), b);
                    addActor(b);
                    i++;
                }
                setupBuildingButtons();
            } else if (newPhase == GamePhase.FIGHTING) {
                background = new Image(new Texture(UIConstants.BG_NIGHT));
                addActor(background);
                for(Map.Entry<Rectangle, Building> i : buildingListMap.entrySet())
                    i.getValue().destroyActor();
                removeBuildingButtons();
                buildingList = new Rectangle(0,0,0,0);
                spellList = new Rectangle(0, 0, getButtonWidth(), getButtonHeight() * defencePhase.getSpellConfigs().size());
                spellListMap = new HashMap<>();
                int i = 0;
                for(SpellType st : defencePhase.getSpellConfigs()) {
                    st.getActor().setHeight(getButtonHeight());
                    st.getActor().setWidth(getButtonWidth());
                    st.setPosition(0, i * getButtonHeight());
                    spellListMap.put(new Rectangle(0, i * getButtonHeight(), getButtonWidth(), getButtonHeight()), st);
                    addActor(st);
                    i++;
                }
            }
            background.setX(0);
            background.setY(0);
            background.setWidth(UIConstants.SCREEN_WIDTH);
            background.setHeight(UIConstants.SCREEN_HEIGHT);
        }
        super.setGameData(gameData);
        lastPhase = newPhase;
        gameData.getBuildingList().forEach(this::addActor);
        addActor(avatar);
        gameData.getEnemyList().forEach(this::addActor);
        gameData.getEffectList().forEach(this::addActor);
    }

    private void setupBuildingButtons() {
        destroyButtonImage.setWidth(getButtonWidth());
        destroyButtonImage.setHeight(getButtonHeight());
        destroyButtonImage.setPosition(0, 0);
        addActor(destroyButtonImage);
        destroyButton = new Rectangle(0, 0, getButtonWidth(), getButtonHeight());
        upgradeButtonImage.setWidth(getButtonWidth());
        upgradeButtonImage.setHeight(getButtonHeight());
        upgradeButtonImage.setPosition(0, getButtonHeight());
        addActor(upgradeButtonImage);
        upgradeButton = new Rectangle(0, getButtonHeight(), getButtonWidth(), 2*getButtonHeight());
    }

    private void removeBuildingButtons() {
        destroyButtonImage.remove();
        destroyButton = new Rectangle(0,0,0,0);
        upgradeButtonImage.remove();
        upgradeButton = new Rectangle(0,0,0,0);
    }

    public void phaseChanged(GamePhase phase) {
        newPhase = phase;
    }

    private int getButtonHeight() {
        return 96;
    }

    private int getButtonWidth() {
        return 64;
    }

    public void addActor(ObjectWithActor a) {
        //UILogger.log("[DD][GameStage]Adding actor: " + a.getClass().getSimpleName());
        addActor(a.getActor());
    }
}
