package com.to2gaming.thelastminer.ui.stages;

import com.badlogic.gdx.scenes.scene2d.Stage;
import com.to2gaming.thelastminer.manager.GameData;

public abstract class MinerStage extends Stage {
    protected GameData gameData;

    public void setGameData(GameData gameData) {
        this.gameData = gameData;
    }
}
