package com.to2gaming.thelastminer.ui.stages;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.to2gaming.thelastminer.TheLastMinerGame;
import com.to2gaming.thelastminer.ui.util.UIConstants;

/**
 * Created by Wladimir Putin on 18.01.16.
 */

public class DeadStage extends MinerStage {
    private Image background;
    private TheLastMinerGame game;

    public DeadStage(TheLastMinerGame manager) {
        this.game = manager;
        background = new Image(new Texture(UIConstants.BG_DEAD));
        background.setX(0);
        background.setY(0);
        background.setWidth(UIConstants.SCREEN_WIDTH);
        background.setHeight(UIConstants.SCREEN_HEIGHT);
        background.addListener(new ClickListener() {
            private int clicks = 3;
            public boolean touchDown(InputEvent e, float x, float y, int pointer, int button) {
                clicks--;
                if(0 > clicks)
                    Gdx.app.exit();
                return true;
            }
        });
    }

    @Override
    public boolean touchDown(int x, int y, int pointer, int button) {
        return super.touchDown(x,y,pointer,button);
    }

    @Override
    public void draw() {
        addActor(background);
        super.draw();
    }
}
