package com.to2gaming.thelastminer.ui.stages;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.to2gaming.thelastminer.TheLastMinerGame;
import com.to2gaming.thelastminer.manager.Difficulty;
import com.to2gaming.thelastminer.ui.util.UIConstants;

public class StartStage extends MinerStage {
    private Image background, play, exit, about;
    private TheLastMinerGame game;

    public StartStage(TheLastMinerGame manager) {
        this.game = manager;
        background = new Image(new Texture(UIConstants.BG_MENU));
        background.setX(0);
        background.setY(0);
        background.setWidth(UIConstants.SCREEN_WIDTH);
        background.setHeight(UIConstants.SCREEN_HEIGHT);
        play = new Image(new Texture(UIConstants.MENU_PLAY));
        play.setX(250);
        play.setY(200);
        play.setWidth(144);
        play.setHeight(96);
        play.addListener(new ClickListener() {
           public boolean touchDown(InputEvent e, float x, float y, int pointer, int button) {
               //TODO startGame stub
               game.startGame("Janusz", Difficulty.MEDIUM);
               //DONE at UIModule.render(): ui.startGame();
               return true;
           }
        });
        exit = new Image(new Texture(UIConstants.MENU_EXIT));
        exit.setX(100);
        exit.setY(200);
        exit.setWidth(144);
        exit.setHeight(96);
        about = new Image(new Texture(UIConstants.MENU_ABOUT));
        about.setX(400);
        about.setY(200);
        about.setWidth(144);
        about.setHeight(96);
    }

    @Override
    public boolean touchDown(int x, int y, int pointer, int button) {
        return super.touchDown(x,y,pointer,button);
    }


    @Override
    public void draw() {
        addActor(background);
        addActor(play);
        addActor(exit);
        addActor(about);
        super.draw();
    }
}
