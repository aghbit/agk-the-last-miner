package com.to2gaming.thelastminer.ui;

import com.badlogic.gdx.Gdx;
import com.to2gaming.thelastminer.TheLastMinerGame;
import com.to2gaming.thelastminer.manager.GameData;
import com.to2gaming.thelastminer.manager.GamePhase;
import com.to2gaming.thelastminer.phases.phaseBuilding.BuildingPhase;
import com.to2gaming.thelastminer.phases.phaseDefence.DefencePhase;
import com.to2gaming.thelastminer.ui.screens.DeadScreen;
import com.to2gaming.thelastminer.ui.screens.GameScreen;
import com.to2gaming.thelastminer.ui.screens.MinerScreen;
import com.to2gaming.thelastminer.ui.screens.StartScreen;

public class UIModule {
    private TheLastMinerGame game;
    private MinerScreen screen;
    private BuildingPhase buildingPhase;
    private DefencePhase defencePhase;
    private GamePhase lastPhase;

    public UIModule(TheLastMinerGame game, BuildingPhase buildingPhase, DefencePhase defencePhase) {
        this.game = game;
        this.buildingPhase = buildingPhase;
        this.defencePhase = defencePhase;
        lastPhase = GamePhase.START;
        screen = new StartScreen(game);
        game.setScreen(screen);
    }

    public void render(GameData gameData, GamePhase phase) {
        if (phase != lastPhase) {
            switch (phase) {
                case START:
                    screen = new StartScreen(game);
                    game.setScreen(screen);
                    break;
                case BUILDING:
                case FIGHTING:
                    if (lastPhase == GamePhase.START)
                        startGame();
                    else
                        screen.phaseChanged(phase);
                    break;
                default:
                    break;
            }
        }
        screen.setGameData(gameData);
        screen.render(Gdx.graphics.getDeltaTime());
        lastPhase = phase;
    }

    public void startGame() {
        screen = new GameScreen(buildingPhase, defencePhase, game);
        game.setScreen(screen);
    }

    public void endGame() {
        screen = new DeadScreen(game);
        game.setScreen(screen);
    }
}
