package com.to2gaming.thelastminer.ui.util;

/**
 * Created by Tytanowy Janusz on 21.12.15.
 */
public class UILogger {
    private static UILogger uiLogger;
//todo fixme trololo
    private UILogger() {
    }

    public static UILogger getInstance() {
        if (uiLogger == null)
            uiLogger = new UILogger();
        return uiLogger;
    }

    public void debug(String message) {
        System.out.println("[DD][UI]" + message);
    }

    public static void log(String message) {
        getInstance().debug(message);
    }
}
