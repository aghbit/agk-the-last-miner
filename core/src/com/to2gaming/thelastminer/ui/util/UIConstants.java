package com.to2gaming.thelastminer.ui.util;

public class UIConstants {
    public static final String BG_DAY = "villages/village1.png";
    public static final String BG_NIGHT = "villages/village2.png";
    public static final String BG_DEAD = "deadscene.png";
    public static final String BG_MENU = "menuBackground.png";
    public static final String MENU_PLAY = "play.png";
    public static final String MENU_EXIT = "exit.png";
    public static final String MENU_ABOUT = "about.png";
    public static final String BUTTON_UPGRADE = "update.png";
    public static final String BUTTON_DESTROY = "destroy.png";

    public static final float BUILD_FAILED_LIFE = 1;
    public static final float COIN_LIFE = 10;
    public static final int MAXIMUM_LAYER = 200;
    public static final int INTERFACE_LAYER = 250;

    public static final int SCREEN_WIDTH = 1024;
    public static final int SCREEN_HEIGHT = 768; //check config.properties

    public static final int AVATAR_WIDTH = 32;
    public static final int AVATAR_HEIGHT = 32;
}
